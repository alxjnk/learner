import { takeLatest } from 'redux-saga/effects';
import {
  AUTH_LOGOUT,
  AUTH_REFRESH_TOKEN_REQUEST,
  AUTH_REQUEST,
} from './modules/auth/actionTypes';
import handleAuthorization, {
  handleLogOutSaga,
  handleRefreshToken,
} from './modules/auth/saga';

export function* rootSaga() {
  yield takeLatest(AUTH_REQUEST, handleAuthorization);
  yield takeLatest(AUTH_REFRESH_TOKEN_REQUEST, handleRefreshToken);
  yield takeLatest(AUTH_LOGOUT, handleLogOutSaga);
}
