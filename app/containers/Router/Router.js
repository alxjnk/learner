import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { allUserPageList } from './pageList';
import {
  getIsAuthorized,
  getIsUser,
  getIsUserAdmin,
  getIsUserStore,
} from '../../modules/auth/selectors';

const Router = () => {
  const pageList = allUserPageList;

  return (
    <Switch>
      {Object.keys(pageList).map(key => (
        <Route
          key={key}
          path={pageList[key].path}
          exact={pageList[key].exact}
          component={pageList[key].component}
        />
      ))}
    </Switch>
  );
};

const mapStateToProps = state => ({
  isAuthorized: getIsAuthorized(state),
  isAdmin: getIsUserAdmin(state),
  isStore: getIsUserStore(state),
  isUser: getIsUser(state),
});

export default connect(mapStateToProps)(Router);
