import NotFoundPage from '../NotFoundPage/Loadable';
import HomePage from '../HomePage';
import CardsPage from '../CardsPage';

export const allUserPageList = {
  main: {
    id: 'home',
    path: '/',
    title: 'home',
    component: HomePage,
    exact: true,
  },
  words: {
    id: 'cards',
    path: '/cards',
    title: 'cards',
    component: CardsPage,
  },
  nf: {
    id: '404',
    title: '404',
    component: NotFoundPage,
  },
};
