/*
 *
 * addEmployee actions
 *
 */

export const GET_USERS_LIST = 'app/AddEmployeePage/GET_USERS_LIST';
export function getUsersListAction() {
  return {
    type: GET_USERS_LIST,
  };
}

export const SET_USERS_LIST = 'app/AddEmployeePage/SET_USERS_LIST';
export function setUsersListAction(payload) {
  return {
    type: SET_USERS_LIST,
    payload,
  };
}
export const POST_USERS_LIST = 'app/AddEmployeePage/POST_USERS_LIST';
export function postUsersListAction() {
  return {
    type: POST_USERS_LIST,
  };
}

export const SET_INPUT = 'app/AddEmployeePage/SET_INPUT';
export function setInputAction(payload) {
  return {
    type: SET_INPUT,
    payload,
  };
}
export const SET_SECTION = 'app/AddEmployeePage/SET_SECTION';
export function setSectionAction(payload) {
  return {
    type: SET_SECTION,
    payload,
  };
}
export const SET_USERS_WITH_CHANGES =
  'app/AddEmployeePage/SET_USERS_WITH_CHANGES';
export function setUserWithChangesAction(payload) {
  return {
    type: SET_USERS_WITH_CHANGES,
    payload,
  };
}
export const PATCH_USERS = 'app/AddEmployeePage/PATCH_USERS';
export function patchUsersAction() {
  return {
    type: PATCH_USERS,
  };
}
export const POST_USERS_LIST_DONE = 'app/AddEmployeePage/POST_USERS_LIST_DONE';
export function postUsersListDoneAction() {
  return {
    type: POST_USERS_LIST_DONE,
  };
}
