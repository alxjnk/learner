import { call, put, select, takeEvery, all, take } from 'redux-saga/effects';
import { v4 as uuidv4 } from 'uuid';
import { notificationInit } from '../../modules/notifications/actions';
import { getTokens } from '../../modules/auth/selectors';
import {
  getSectionRequest,
  getUsersListRequest,
  patchSectionWorkersRequest,
  patchUserSectionIdRequest,
} from '../../utils/api/requests';
import history from '../../utils/history';
import {
  GET_USERS_LIST,
  POST_USERS_LIST,
  setUsersListAction,
  setSectionAction,
  patchUsersAction,
  PATCH_USERS,
  postUsersListDoneAction,
} from './actions';
import {
  getUsersForPatchListSelector,
  getSectionId,
  getAllUsersWithChangesForPatchSelector,
} from './selectors';
import { AUTH_REFRESH_TOKEN } from '../../modules/auth/actionTypes';
import { authRefreshTokenRequest } from '../../modules/auth/actions';
import { getIsTokenExpiresSelector } from '../App/selectors';

function* getUsersListSaga() {
  try {
    const isTokenExpired = yield select(getIsTokenExpiresSelector());
    if (isTokenExpired) {
      yield put(authRefreshTokenRequest());
      yield take(AUTH_REFRESH_TOKEN);
    }
    const { access } = yield select(getTokens);
    const users = yield call(getUsersListRequest, access.token);
    const sectionId = yield select(getSectionId);
    const section = yield call(getSectionRequest, sectionId, access.token);
    yield put(setSectionAction(section));
    yield put(setUsersListAction(users));
  } catch (error) {
    yield put(
      notificationInit({
        id: uuidv4(),
        message: error.message,
        type: 'warning',
        dismissAfter: 3000,
      }),
    );
  }
}

function* setUsersListSaga() {
  try {
    const { access } = yield select(getTokens);
    const usersList = yield select(getUsersForPatchListSelector());
    const sectionId = yield select(getSectionId);
    const result = yield call(
      patchSectionWorkersRequest,
      usersList,
      sectionId,
      access.token,
    );
    if (result.code) {
      throw new Error(result.message);
    }
    yield put(patchUsersAction());
  } catch (error) {
    yield put(
      notificationInit({
        id: uuidv4(),
        message: error.message,
        type: 'warning',
        dismissAfter: 3000,
      }),
    );
  }
}

function* patchUsersSectionSaga() {
  try {
    const { access } = yield select(getTokens);
    const arrayWithUsers = yield select(
      getAllUsersWithChangesForPatchSelector(),
    );
    const result = yield all(
      arrayWithUsers.map(el =>
        call(
          patchUserSectionIdRequest,
          {
            sectionId: el.sectionId,
          },
          el.userId,
          access.token,
        ),
      ),
    );
    if (result.code) {
      yield put(patchUsersAction());
    }
    yield put(postUsersListDoneAction());
    history.push('/stats');
  } catch (error) {
    yield put(
      notificationInit({
        id: uuidv4(),
        message: error.message,
        type: 'warning',
        dismissAfter: 3000,
      }),
    );
  }
}

// Individual exports for testing
export default function* addEmployeeSaga() {
  yield takeEvery(GET_USERS_LIST, getUsersListSaga);
  yield takeEvery(POST_USERS_LIST, setUsersListSaga);
  yield takeEvery(PATCH_USERS, patchUsersSectionSaga);
}
