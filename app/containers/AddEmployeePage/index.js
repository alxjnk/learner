import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useTranslation } from 'react-i18next';
import { Loader } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import history from '../../utils/history';
import saga from './saga';
import reducer from './reducer';
import { Layout } from '../../components/layout';
import { Header } from '../../components/header/header';
import {
  getUsersListAction,
  setUsersListAction,
  postUsersListAction,
  setInputAction,
} from './actions';
import {
  getUsersListSelector,
  getAllUsersListSelector,
  getSectionId,
  getInputTextSelector,
  getSectionNameSelector,
  getUsersWithFilterSelector,
  getIsLoadingSelector,
  getIsDataSendingSelector,
} from './selectors';
import { AddEmployeeContent } from './components/content';
import { FooterButton } from '../../components/footer-button';

function AddEmployee({
  onGetUsersList,
  usersList,
  onSetUsersList,
  sectionId,
  onPostUsersList,
  inputText,
  onSetInput,
  sectionName,
  usersListWithFilter,
  isLoading,
  isDataSending,
}) {
  useInjectSaga({ key: 'addEmployee', saga });
  useInjectReducer({ key: 'addEmployee', reducer });
  useEffect(() => {
    onGetUsersList();
  }, []);
  const handleClickBack = () => {
    history.push('/stats');
  };
  const handleWorkerClick = id => {
    onSetUsersList(
      usersList.map(el => {
        if (el.id !== id) return el;
        return {
          ...el,
          sectionId: el.sectionId ? '' : sectionId,
          isChanged: true,
        };
      }),
    );
  };
  const handleConfirm = () => {
    onPostUsersList();
  };
  const handleInputChange = e => {
    onSetInput(e.target.value);
  };

  const content = isLoading ? (
    <Loader active size="massive" />
  ) : (
    <AddEmployeeContent
      data={usersListWithFilter}
      onConfirm={handleWorkerClick}
      inputText={inputText}
      onInputChange={handleInputChange}
    />
  );
  const { t } = useTranslation();
  return (
    <Layout
      header={<Header title={sectionName} onBackClick={handleClickBack} />}
      content={content}
      footer={
        <FooterButton
          text={t('CONFIRM EMPLOYEES')}
          onClick={handleConfirm}
          loading={isLoading || isDataSending}
        />
      }
      footerHeight="60px"
    />
  );
}

AddEmployee.propTypes = {
  onGetUsersList: PropTypes.func.isRequired,
  usersList: PropTypes.arrayOf(
    PropTypes.shape({
      role: PropTypes.string.isRequired,
      onShiftToday: PropTypes.bool.isRequired,
      sectionId: PropTypes.string,
      id: PropTypes.string.isRequired,
    }),
  ),
  onSetUsersList: PropTypes.func.isRequired,
  sectionId: PropTypes.string,
  onPostUsersList: PropTypes.func.isRequired,
  inputText: PropTypes.string.isRequired,
  onSetInput: PropTypes.func.isRequired,
  sectionName: PropTypes.string,
  usersListWithFilter: PropTypes.arrayOf(
    PropTypes.shape({
      role: PropTypes.string.isRequired,
      onShiftToday: PropTypes.bool.isRequired,
      sectionId: PropTypes.string,
      tasks: PropTypes.array,
      id: PropTypes.string.isRequired,
    }),
  ),
  isLoading: PropTypes.bool.isRequired,
  isDataSending: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  usersList: getUsersListSelector(),
  allUsersList: getAllUsersListSelector(),
  sectionId: getSectionId,
  inputText: getInputTextSelector(),
  sectionName: getSectionNameSelector(),
  usersListWithFilter: getUsersWithFilterSelector(),
  isLoading: getIsLoadingSelector(),
  isDataSending: getIsDataSendingSelector(),
});
const mapDispatchToProps = {
  onGetUsersList: getUsersListAction,
  onSetUsersList: setUsersListAction,
  onPostUsersList: postUsersListAction,
  onSetInput: setInputAction,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const AddEmployeePage = compose(withConnect)(AddEmployee);

export default AddEmployeePage;
