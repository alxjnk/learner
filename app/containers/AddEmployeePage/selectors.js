import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the addEmployee state domain
 */

const selectAddEmployeePageDomain = state => state.addEmployee || initialState;
const selectRouterDomain = state => state.router;

/**
 * Other specific selectors
 */

/**
 * Default selector used by addEmployee
 */

const makeSelectAddEmployeePage = () =>
  createSelector(
    selectAddEmployeePageDomain,
    substate => substate,
  );

const getUsersListSelector = () =>
  createSelector(
    selectAddEmployeePageDomain,
    getSectionId,
    ({ usersList }, id) =>
      usersList.reduce((acc, el) => {
        if (el.role === 'admin') return acc;
        // if item section id === id current section, item goes first
        if (el.sectionId === id) return [el, ...acc];
        // if item section id === empty string, item goes to the bottom of list
        if (!el.sectionId) return [...acc, el];
        return acc;
      }, []),
  );

const getAllUsersListSelector = () =>
  createSelector(
    selectAddEmployeePageDomain,
    ({ usersList }) => usersList,
  );

const getUsersForPatchListSelector = () =>
  createSelector(
    selectAddEmployeePageDomain,
    getSectionId,
    ({ usersList }, id) => ({
      users: usersList.reduce(
        (acc, el) => (el.sectionId === id ? [...acc, el.id] : acc),
        [],
      ),
    }),
  );

const getInputTextSelector = () =>
  createSelector(
    selectAddEmployeePageDomain,
    ({ input }) => input,
  );

const getSectionNameSelector = () =>
  createSelector(
    selectAddEmployeePageDomain,
    ({ section }) => section.name || '',
  );

const getAllUsersWithChangesForPatchSelector = () =>
  createSelector(
    selectAddEmployeePageDomain,
    ({ usersList }) =>
      usersList.reduce(
        (acc, el) =>
          el.isChanged
            ? [...acc, { userId: el.id, sectionId: el.sectionId }]
            : acc,
        [],
      ),
  );

const getUsersWithFilterSelector = () =>
  createSelector(
    selectAddEmployeePageDomain,
    getUsersListSelector(),
    ({ input }, users) =>
      users.filter(el =>
        `${el.name} ${el.lastname}`.toLowerCase().includes(input.toLowerCase()),
      ),
  );

const getIsLoadingSelector = () =>
  createSelector(
    selectAddEmployeePageDomain,
    ({ isLoading }) => isLoading,
  );

const getIsDataSendingSelector = () =>
  createSelector(
    selectAddEmployeePageDomain,
    ({ isDataSending }) => isDataSending,
  );

const getSectionId = createSelector(
  selectRouterDomain,
  ({ location }) => location.pathname.split('/')[3],
);

export default makeSelectAddEmployeePage;
export {
  selectAddEmployeePageDomain,
  getUsersListSelector,
  getAllUsersListSelector,
  getSectionId,
  getInputTextSelector,
  getUsersForPatchListSelector,
  getSectionNameSelector,
  getAllUsersWithChangesForPatchSelector,
  getUsersWithFilterSelector,
  getIsLoadingSelector,
  getIsDataSendingSelector,
};
