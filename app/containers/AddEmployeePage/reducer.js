/*
 *
 * addEmployee reducer
 *
 */
import { AUTH_LOGOUT_COMPLETE } from '../../modules/auth/actionTypes';
import {
  SET_INPUT,
  SET_USERS_LIST,
  SET_SECTION,
  SET_USERS_WITH_CHANGES,
  GET_USERS_LIST,
  POST_USERS_LIST,
  POST_USERS_LIST_DONE,
} from './actions';

export const initialState = {
  usersList: [],
  input: '',
  section: {},
  usersWithChanges: [],
  isLoading: true,
  isDataSending: false,
};

const addEmployeeReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_LOGOUT_COMPLETE:
      return initialState;
    case SET_USERS_LIST:
      return { ...state, usersList: action.payload, isLoading: false };
    case SET_INPUT:
      return { ...state, input: action.payload };
    case SET_SECTION:
      return { ...state, section: action.payload };
    case SET_USERS_WITH_CHANGES:
      return { ...state, usersWithChanges: action.payload };
    case GET_USERS_LIST:
      return { ...state, isLoading: true };
    case POST_USERS_LIST:
      return { ...state, isDataSending: true };
    case POST_USERS_LIST_DONE:
      return { ...state, isDataSending: false };
    default:
      return state;
  }
};

export default addEmployeeReducer;
