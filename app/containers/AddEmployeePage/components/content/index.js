import React from 'react';
import PropTypes from 'prop-types';
import { Check, Square } from 'tabler-icons-react';
import { useTranslation } from 'react-i18next';
import { Input } from 'semantic-ui-react';
import { MessageInsteadOfContent } from '../../../../components/message-instead-of-content';
import { MobileListItem } from '../../../../my-custom-semantic-theme/components/mobile-list-item';
import { IconWrapper } from '../icon-wrapper';
import './styles.less';

function AddEmployeeContent({ data, onConfirm, inputText, onInputChange }) {
  const { t } = useTranslation();
  return (
    <div className="add-employee-content">
      <div className="add-employee-content__input">
        <Input
          placeholder={t('Search employee...')}
          value={inputText}
          onChange={onInputChange}
        />
      </div>
      {data.length > 0 ? (
        data.map(el => {
          const textColor = el.sectionId ? 'blue' : 'purple';
          const color = el.sectionId ? '#0099FF' : '#EA00FF';
          const icon = el.sectionId ? Check : Square;
          const handleConfirm = () => {
            onConfirm(el.id);
          };
          return (
            <MobileListItem
              key={el.id}
              title={`${el.name} ${el.lastname}`}
              subText={t('Cook')}
              borderColor={textColor}
              onItemTextClick={handleConfirm}
              icon={
                <IconWrapper
                  icon={icon}
                  color={color}
                  id={el.id}
                  onIconClick={handleConfirm}
                />
              }
            />
          );
        })
      ) : (
        <MessageInsteadOfContent message={t('noCookFind')} />
      )}
    </div>
  );
}
AddEmployeeContent.propTypes = {
  onConfirm: PropTypes.func,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      role: PropTypes.string,
      onShiftToday: PropTypes.bool,
      sectionId: PropTypes.string,
      tasks: PropTypes.array,
      id: PropTypes.string,
    }),
  ),
  inputText: PropTypes.string,
  onInputChange: PropTypes.func,
};

export { AddEmployeeContent };
