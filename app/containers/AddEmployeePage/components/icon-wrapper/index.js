import React from 'react';
import PropTypes from 'prop-types';

const IconWrapper = ({ icon: Icon, onIconClick, color, id }) => {
  const handleIconClick = () => {
    onIconClick(id);
  };
  return (
    <Icon
      size={50}
      className="mobile-list-item__info-icon"
      onClick={handleIconClick}
      color={color}
    />
  );
};

IconWrapper.propTypes = {
  icon: PropTypes.elementType.isRequired,
  onIconClick: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export { IconWrapper };
