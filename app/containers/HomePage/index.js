import React from 'react';
import { Loader, List, Segment } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { Layout } from '../../components/layout';
import { Header } from '../../components/header/header';

export default function HomePage() {
  const isLoading = false;
  return (
    <Layout
      header={<Header title="Learner" />}
      content={
        isLoading ? (
          <Loader size="massive" active />
        ) : (
          <Segment style={{ background: 'black', margin: '20vh 5vw' }}>
            <List animated verticalAlign="middle">
              {[
                { title: 'Избранное', icon: 'star' },
                { title: 'Знать Слова', icon: 'adn', link: '/cards' },
                { title: 'Легкая Грамматика', icon: 'compass' },
                { title: 'Слушать аудио', icon: 'file audio' },
                { title: 'Полезные игры', icon: 'game' },
                { title: 'Читать книги', icon: 'book' },
                { title: 'Общаться с людьми', icon: 'comments outline' },
                { title: 'Полезняшки', icon: 'circle' },
              ].map(({ title, icon, link }) => (
                <List.Item href={link}>
                  <List.Icon size="big" name={icon} color="blue" />
                  <List.Content>
                    <List.Header
                      size="big"
                      style={{ color: 'white', background: 'black' }}
                    >
                      <Link to={link}>{title}</Link>
                    </List.Header>
                  </List.Content>
                </List.Item>
              ))}
            </List>
          </Segment>
        )
      }
    />
  );
}
