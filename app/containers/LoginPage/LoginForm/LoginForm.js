/*
 * LoginForm
 */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Button } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';
import { useTranslation } from 'react-i18next';
import { authRequest } from '../../../modules/auth/actions';
import {
  getIsLoading,
  getTokens,
  getErrors,
} from '../../../modules/auth/selectors';
import { removeAllNotNumberSymbolsAndAddPlusToStart, validate } from './utils';
import { FormInputField } from '../../../my-custom-semantic-theme/components/form-input-field';
import { FormPhoneInputField } from '../../../my-custom-semantic-theme/components/form-phone-input-field';
const FORM_NAME = 'loginForm';

const LoginForm = ({
  onAuthRequest,
  handleSubmit,
  isLoading,
  submitting,
  errors,
}) => {
  const onSubmit = values => {
    const { phone, password } = values;
    if (phone && password) {
      onAuthRequest({
        phone: removeAllNotNumberSymbolsAndAddPlusToStart(phone),
        password,
      });
    }
  };
  const { t } = useTranslation();
  return (
    <div className="login_form">
      <form onSubmit={handleSubmit(onSubmit)}>
        <Field
          placeholder={t('login')}
          name="phone"
          type="string"
          component={FormPhoneInputField}
        />
        <Field
          placeholder={t('password')}
          name="password"
          type="password"
          component={FormInputField}
        />
        {errors && <div className="login_form-error">{t(errors)}</div>}
        <Button
          primary
          size="large"
          type="submit"
          disabled={submitting}
          loading={isLoading}
        >
          {t('Log In')}
        </Button>
      </form>
    </div>
  );
};

LoginForm.propTypes = {
  onAuthRequest: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  errors: PropTypes.string,
};

const LoginReduxForm = reduxForm({
  form: FORM_NAME,
  validate,
})(LoginForm);

const mapStateToProps = state => ({
  isLoading: getIsLoading(state),
  tokens: getTokens(state),
  errors: getErrors(state),
});

const mapDispatchToProps = { onAuthRequest: authRequest };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(LoginReduxForm);
