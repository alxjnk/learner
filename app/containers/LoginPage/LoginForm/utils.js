import { removeAllNotNumberSymbolsRegEx } from '../../../utils/regEx';

export const validate = values => {
  const errors = {};
  const { phone, password } = values;

  if (!phone) {
    errors.phone = 'The login field is required';
  }
  if (phone) {
    if (phone.length < 11) {
      errors.phone = 'Your login must be at least 11 characters';
    }
    if (phone.length > 18) {
      errors.phone = "Your login can't be more than 18 characters";
    }
  }

  if (!password) {
    errors.password = 'The password field is required';
  } else if (password && password.length < 3) {
    errors.password = 'Your password must be at least 3 characters. ';
  }

  return errors;
};

export const removeAllNotNumberSymbolsAndAddPlusToStart = str => {
  const numbers = str.replace(removeAllNotNumberSymbolsRegEx, '');
  return `+${numbers}`;
};
