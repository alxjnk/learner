/*
 * LoginPage
 */
import React from 'react';
import Logo from './Logo';
import LoginForm from './LoginForm';
import './styles.less';
import InstallPwaPopup from '../InstallPwaPopup';

export default function LoginPage() {
  return (
    <div className="login_page">
      <Logo />
      <LoginForm />
      <InstallPwaPopup />
    </div>
  );
}
