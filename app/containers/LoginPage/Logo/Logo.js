/*
 * Logo
 *
 */
import React from 'react';
import { KitchenIcon } from '../../../components/kitchen-icon';
import './styles.less';

export default function Logo() {
  return (
    <div className="logo">
      <KitchenIcon />
      <div className="logo-tag-line">Kitchen</div>
    </div>
  );
}
