import { AUTH_LOGOUT_COMPLETE } from '../../modules/auth/actionTypes';
import {
  SET_USER,
  GET_USER,
  SET_CURRENT_CARD_ID,
  SET_CURRENT_CARDS,
} from './actions';
import { a2VocabularyCards } from './cards';

export const initialState = {
  user: {},
  cardsData: JSON.parse(localStorage.getItem('cards')) || a2VocabularyCards,
  currentCardId: 0,
  isLoading: true,
};

const cookTasksReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_LOGOUT_COMPLETE:
      return initialState;
    case SET_CURRENT_CARD_ID:
      return { ...state, currentCardId: action.payload };
    case SET_CURRENT_CARDS:
      return { ...state, cardsData: action.payload };
    case SET_USER:
      return { ...state, user: action.payload, isLoading: false };
    case GET_USER:
      return { ...state, isLoading: true };
    default:
      return state;
  }
};

export default cookTasksReducer;
