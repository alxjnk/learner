import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './styles.less';
import { Button, Icon, Label, Image } from 'semantic-ui-react';
import { animated } from '@react-spring/web';

function CardsContent({
  data,
  onReadText,
  setShowDetails,
  showDetails,
  setShowDetailsButtons,
  showDetailsButtons,
  showAccociations,
  setShowAccociations,
  bind,
  x,
  y,
  i,
}) {
  const [translationShown, setTranslationShown] = useState(false);

  const handleShowDetailsButtons = () => {
    setShowDetailsButtons(!showDetailsButtons);
  };

  const handleShowDetails = () => {
    setShowDetails(!showDetails);
    if (showAccociations) {
      setShowAccociations(!showAccociations);
    }
  };

  const handleShowAccociations = () => {
    setShowAccociations(!showAccociations);
    if (showDetails) {
      setShowDetails(!showDetails);
    }
  };

  let cardContent = null;

  if (showDetails) {
    cardContent = (
      <>
        <div
          style={{
            margin: '0 10px 10px 10px',
            fontSize: 12,
            fontStyle: 'italic',
          }}
        >
          <div>
            <Label size="mini" color="green">
              Popularity
              <Label.Detail>{data.details.popularityIndex}</Label.Detail>
            </Label>
          </div>
          {data.details.definition}
        </div>
        <div
          style={{
            margin: '0 10px 10px 10px',
            fontSize: 12,
          }}
        >
          Синонимы: {data.details.synonyms.join(', ')}
        </div>
        <div
          style={{
            margin: '0 10px 10px 10px',
            fontSize: 12,
          }}
        >
          Предложения:{' '}
          {data.details.sentences.map(sentence => (
            <div>- {sentence}</div>
          ))}
        </div>
      </>
    );
  }

  if (showAccociations) {
    cardContent = (
      <>
        <div
          style={{
            margin: '0 10px 10px 10px',
            fontSize: 12,
            fontStyle: 'italic',
          }}
        >
          <div>
            <Label size="mini" color="green">
              Popularity
              <Label.Detail>{data.details.popularityIndex}</Label.Detail>
            </Label>
          </div>
          {data.details.definition}
        </div>
        <div>
          <Image
            src="https://react.semantic-ui.com/images/wireframe/image.png"
            size="small"
          />
        </div>
        <div style={{ marginBottom: 10 }}>
          Ассоциация: {data.associations.text}
        </div>
      </>
    );
  }

  const handleClick = event => {
    if (event.target.tagName === 'DIV') {
      setTranslationShown(!translationShown);
      if (showDetails) {
        setShowDetails(!showDetails);
      }
      if (showAccociations) {
        setShowAccociations(!showAccociations);
      }
    }
  };

  return (
    <animated.div
      key={i}
      onClick={handleClick}
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        position: 'absolute',
        height: '80%',
        width: '95%',
        touchAction: 'none',
        willChange: 'transform',
        // transform: 'none',
        x,
        y,
      }}
    >
      <animated.div
        {...bind(i)}
        style={{
          cursor: 'pointer',
          textAlign: 'center',
          flexGrow: 80,
          display: 'flex',
          flexDirection: 'column',
          border: '1px solid #2185D0',
          borderRadius: 10,
          margin: '0px 10px 10px 10px',
          background: '#26282E',
          touchAction: 'none',
          willChange: 'transform',
        }}
      >
        {data ? (
          <>
            <div
              style={{
                flexGrow: 1,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-end',
              }}
            >
              <h1>{!translationShown ? data.word : data.translation}</h1>
              {translationShown && (
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                  <Image
                    src="https://react.semantic-ui.com/images/wireframe/image.png"
                    size="small"
                  />
                </div>
              )}
            </div>
            <div
              style={{
                display: 'flex',
                flexGrow: 1,
                justifyContent: 'flex-start',
                flexDirection: 'column',
                alignItems: 'center',
                marginTop: 10,
              }}
            >
              {cardContent}
              <Button
                icon
                basic
                color="blue"
                style={{ width: 50 }}
                onClick={onReadText}
              >
                <Icon name="play" color="blue" />
              </Button>
            </div>
          </>
        ) : (
          <div
            style={{
              flexGrow: 1,
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
            }}
          >
            <h1>Поздравляем, у Тебя НЕТ карточек для изучения</h1>
          </div>
        )}

        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-start',
              marginBottom: 10,
              flexGrow: 6,
              paddingLeft: 10,
            }}
          >
            {showDetailsButtons ? (
              <>
                <Button
                  icon
                  basic
                  color="blue"
                  style={{ width: 50 }}
                  // onClick={handleShowDetails}
                >
                  <Icon name="arrows alternate" color="blue" />
                </Button>
                <Button
                  icon
                  basic
                  color="blue"
                  style={{ width: 50 }}
                  onClick={handleShowDetails}
                >
                  <Icon name="list alternate outline" color="blue" />
                </Button>
                <Button
                  icon
                  basic
                  color="blue"
                  style={{ width: 50 }}
                  onClick={handleShowAccociations}
                >
                  A
                </Button>
              </>
            ) : null}
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              marginBottom: 10,
              flexGrow: 6,
              marginRight: 5,
            }}
          >
            <Button
              icon
              basic
              color="blue"
              style={{ width: 50 }}
              onClick={handleShowDetailsButtons}
            >
              <Icon
                name={showDetailsButtons ? 'expand' : 'expand arrows alternate'}
                color="blue"
              />
            </Button>
          </div>
        </div>
      </animated.div>
    </animated.div>
  );
}
CardsContent.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      finished: PropTypes.bool.isRequired,
      id: PropTypes.string.isRequired,
      receipt: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        weight: PropTypes.number.isRequired,
      }),
      receiptQuantity: PropTypes.number.isRequired,
    }),
  ),
  onNextCard: PropTypes.func.isRequired,
  onPrevCard: PropTypes.func.isRequired,
  onReadText: PropTypes.func.isRequired,
  setShowDetails: PropTypes.func.isRequired,
  setShowDetailsButtons: PropTypes.func.isRequired,
  showDetails: PropTypes.bool.isRequired,
  showDetailsButtons: PropTypes.bool.isRequired,
  showAccociations: PropTypes.bool.isRequired,
  setShowAccociations: PropTypes.func.isRequired,
};

export { CardsContent };
