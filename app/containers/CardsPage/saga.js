import { call, put, select, takeEvery } from 'redux-saga/effects';
import { v4 as uuidv4 } from 'uuid';
import { notificationInit } from '../../modules/notifications/actions';
import { getTokens } from '../../modules/auth/selectors';
import { getUserRequest } from '../../utils/api/requests';
import { GET_USER, setUser } from './actions';
import { getCurrentCookId } from './selectors';

function* getUserSaga() {
  try {
    const { access } = yield select(getTokens);
    const id = yield select(getCurrentCookId());
    const result = yield call(getUserRequest, id, access.token);

    yield put(setUser(result));
  } catch (error) {
    yield put(
      notificationInit({
        id: uuidv4(),
        message: error.message,
        type: 'warning',
        dismissAfter: 3000,
      }),
    );
  }
}

// Individual exports for testing
export default function* cookTasksSaga() {
  yield takeEvery(GET_USER, getUserSaga);
}
