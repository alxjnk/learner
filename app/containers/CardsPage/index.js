import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import moment from 'moment';
import { useSprings } from '@react-spring/web';
import { useDrag } from '@use-gesture/react';
import { Button, Icon } from 'semantic-ui-react';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import history from '../../utils/history';
import saga from './saga';
import reducer from './reducer';
import { Layout } from '../../components/layout';
import { Header } from '../../components/header/header';
import {
  getCardsIdDataSelector,
  getCurrentCardIdSelector,
  getCurrentCookId,
  getIsLoadingSelector,
  getUserNameSelector,
} from './selectors';
import { CardsContent } from './components/content';
import {
  getUser,
  setCurrentCardIdAction,
  setCurrentCardsAction,
} from './actions';

function CookTasks({
  currentCardId,
  cardsData,
  onSetCurrentCardId,
  onSetCardsData,
}) {
  useInjectSaga({ key: 'cookTasksPage', saga });
  useInjectReducer({ key: 'cookTasksPage', reducer });

  const [showDetails, setShowDetails] = useState(false);
  const [showDetailsButtons, setShowDetailsButtons] = useState(false);
  const [showAccociations, setShowAccociations] = useState(false);

  useEffect(() => {
    onSetCurrentCardId(cardsData.length - 1);
  }, []);

  const handleBackButton = () => {
    history.push('/');
  };

  const handleNextCard = hours => {
    const currentCardDate = moment();

    const newCardDate = currentCardDate.add(hours, 'hours');

    const newCard = cardsData[currentCardId];
    newCard.nextShowDate = newCardDate;
    let newCards = [...cardsData];
    newCards[currentCardId] = newCard;
    newCards = newCards.sort((a, b) => {
      if (moment(a.nextShowDate) < moment(b.nextShowDate)) {
        return -1;
      }
      return 1;
    });

    onSetCardsData(newCards);
    localStorage.setItem('cards', JSON.stringify(newCards));

    const newIndex = cardsData.findIndex(
      card => moment(card.nextShowDate) <= moment(),
    );

    onSetCurrentCardId(newIndex);
  };

  const handlePrevCard = minutes => {
    const currentCardDate = moment();

    const newCardDate = currentCardDate.add(minutes, 'minutes');

    const newCard = cardsData[currentCardId];
    newCard.nextShowDate = newCardDate;
    let newCards = [...cardsData];
    newCards[currentCardId] = newCard;
    newCards = newCards.sort((a, b) => {
      if (moment(a.nextShowDate) < moment(b.nextShowDate)) {
        return -1;
      }
      return 1;
    });
    onSetCardsData(newCards);
    localStorage.setItem('cards', JSON.stringify(newCards));

    const newIndex = cardsData.findIndex(
      card => moment(card.nextShowDate) <= moment(),
    );

    onSetCurrentCardId(newIndex);
  };

  const handleReadText = () => {
    const msg = new SpeechSynthesisUtterance();
    if (showDetails) {
      msg.text = `${cardsData[currentCardId].word}. Definition, ${
        cardsData[currentCardId].details.definition
      }, examples, ${cardsData[currentCardId].details.sentences.join(' ')}`;
    } else {
      msg.text = cardsData[currentCardId].word;
    }

    window.speechSynthesis.speak(msg);
  };

  // spring
  const to = i => ({
    x: 0,
    y: i * 0,
    // delay: i * 100,
  });
  const from = _i => ({ x: 0, rot: 0, scale: 1.5, y: -1000 });

  const [gone] = useState(() => new Set()); // The set flags all the cards that are flicked out
  const [props, api] = useSprings(cardsData.length, i => ({
    ...to(i),
    from: from(i),
  })); // Create a bunch of springs using the helpers above

  // Create a gesture, we're interested in down-state, delta (current-pos - click-pos), direction and velocity
  const bind = useDrag(
    ({
      args: [index],
      active,
      movement: [mx],
      direction: [xDir],
      velocity: [vx],
    }) => {
      const trigger = vx > 0.2; // If you flick hard enough it should trigger the card to fly out
      if (!active && trigger) gone.add(index); // If button/finger's up and trigger velocity is reached, we flag the card ready to fly out
      api.start(i => {
        if (index !== i) return; // We're only interested in changing spring-data for the current spring
        const isGone = gone.has(index);
        const x = isGone ? (100 + window.innerWidth) * xDir : active ? mx : 0; // When a card is gone it flys out left or right, otherwise goes back to zero
        if (isGone) {
          if (xDir < 0) {
            handlePrevCard(6);
          } else {
            handleNextCard(2);
          }
          onSetCurrentCardId(index - 1);
        }
        return {
          x,
          delay: undefined,
          config: { friction: 50, tension: active ? 800 : isGone ? 100 : 500 },
        };
      });
    },
  );

  return (
    <Layout
      header={<Header onBackClick={handleBackButton} title="Слова" />}
      content={
        cardsData.length === 0 ? (
          <div
            style={{
              flexGrow: 1,
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              height: '50%',
              padding: '10px 10px 10px 10px',
              textAlign: 'center',
            }}
          >
            <h1>Поздравляем, у Тебя НЕТ карточек для изучения</h1>
          </div>
        ) : (
          props.map(({ x, y }, i) => (
            <CardsContent
              i={i}
              bind={bind}
              data={cardsData[currentCardId]}
              x={x}
              y={y}
              onReadText={handleReadText}
              showDetails={showDetails}
              setShowDetails={setShowDetails}
              showDetailsButtons={showDetailsButtons}
              setShowDetailsButtons={setShowDetailsButtons}
              showAccociations={showAccociations}
              setShowAccociations={setShowAccociations}
            />
          ))
        )
      }
      footer={
        <div
          style={{
            display: 'flex',
            flexGrow: 1,
            margin: '0 20px 10px 20px',
            justifyContent: 'space-between',
          }}
        >
          <div style={{ width: 50 }}>
            <Button icon basic color="red" onClick={() => handlePrevCard(1)}>
              <Icon name="chevron left" />
            </Button>
            <div style={{ textAlign: 'center' }}>1 мин</div>
          </div>
          <div style={{ width: 50 }}>
            <Button icon basic color="red" onClick={() => handlePrevCard(6)}>
              <Icon name="angle double left" />
            </Button>
            <div style={{ textAlign: 'center' }}> 6 мин</div>
          </div>
          <div style={{ width: 50 }}>
            <Button icon basic color="green" onClick={() => handleNextCard(1)}>
              <Icon name="chevron right" />
            </Button>
            <div style={{ textAlign: 'center' }}>1 час</div>
          </div>
          <div style={{ width: 50 }}>
            <Button icon basic color="green" onClick={() => handleNextCard(48)}>
              <Icon name="angle double right" />
            </Button>
            <div style={{ textAlign: 'center' }}>2 дня</div>
          </div>
        </div>
      }
      footerHeight="60px"
    />
  );
}

CookTasks.propTypes = {
  onSetCurrentCardId: PropTypes.func.isRequired,
  currentCardId: PropTypes.number.isRequired,
  cardsData: PropTypes.arrayOf(
    PropTypes.shape({
      finished: PropTypes.bool,
      id: PropTypes.string.isRequired,
      receipt: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        weight: PropTypes.number.isRequired,
      }),
      receiptQuantity: PropTypes.number,
    }),
  ),
  onSetCardsData: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  currentCookId: getCurrentCookId(),
  currentCookName: getUserNameSelector(),
  isLoading: getIsLoadingSelector(),
  cardsData: getCardsIdDataSelector(),
  currentCardId: getCurrentCardIdSelector(),
});
const mapDispatchToProps = {
  onGetUser: getUser,
  onSetCurrentCardId: setCurrentCardIdAction,
  onSetCardsData: setCurrentCardsAction,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const CookTasksPage = compose(withConnect)(CookTasks);

export default CookTasksPage;
