/*
 *
 * CookTasksPage actions
 *
 */

export const CHANGE_TASK_COMPLETE_FIELD =
  'app/cookTasksPage/CHANGE_TASK_COMPLETE_FIELD';
export function setChangeTaskIsComplete(payload) {
  return {
    type: CHANGE_TASK_COMPLETE_FIELD,
    payload,
  };
}

export const GET_USER = 'app/cookTasksPage/GET_USER';
export function getUser() {
  return {
    type: GET_USER,
  };
}

export const SET_USER = 'app/cookTasksPage/SET_USER';
export function setUser(payload) {
  return {
    type: SET_USER,
    payload,
  };
}

export const SET_CURRENT_CARD_ID = 'app/cookTasksPage/SET_CURRENT_CARD_ID';
export function setCurrentCardIdAction(payload) {
  return {
    type: SET_CURRENT_CARD_ID,
    payload,
  };
}

export const SET_CURRENT_CARDS = 'app/cookTasksPage/SET_CURRENT_CARDS';
export function setCurrentCardsAction(payload) {
  return { type: SET_CURRENT_CARDS, payload };
}
