import { createSelector } from 'reselect';
import moment from 'moment';
import { initialState } from './reducer';

/**
 * Direct selector to the cookTasksPage state domain
 */

const cookTasksDomain = state => state.cookTasksPage || initialState;
const selectRouterDomain = state => state.router;

/**
 * Other specific selectors
 */

/**
 * Default selector used by CookTasksPage
 */

const makeCookTasksPage = () =>
  createSelector(
    cookTasksDomain,
    substate => substate,
  );
const getCurrentCookId = () =>
  createSelector(
    selectRouterDomain,
    ({ location }) => location.pathname.split('/')[2],
  );

const getUserNameSelector = () =>
  createSelector(
    cookTasksDomain,
    ({ user }) => (user.name ? `${user.name} ${user.lastname}` : ''),
  );
const getUserTasksSelector = () =>
  createSelector(
    cookTasksDomain,
    ({ user }) => user.tasks,
  );

const getIsLoadingSelector = () =>
  createSelector(
    cookTasksDomain,
    ({ isLoading }) => isLoading,
  );

const getCardsIdDataSelector = () =>
  createSelector(
    cookTasksDomain,
    ({ cardsData }) =>
      cardsData.filter(card => moment(card.nextShowDate) <= moment.now()),
  );

const getCurrentCardIdSelector = () =>
  createSelector(
    cookTasksDomain,
    ({ currentCardId }) => currentCardId,
  );

export default makeCookTasksPage;
export {
  getCurrentCookId,
  getUserNameSelector,
  getUserTasksSelector,
  getIsLoadingSelector,
  getCardsIdDataSelector,
  getCurrentCardIdSelector,
};
