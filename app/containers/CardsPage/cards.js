export const a2VocabularyCards = [
  {
    word: 'Friend',
    translation: 'Друг',
    image: 'friend.jpg',
    audio: 'friend.mp3',
    nextShowDate: '2023-10-06T08:00:00',
    nextReviewLevel: 1,
    associations: {
      image: 'friend_association.jpg',
      text: 'Companion',
    },
    details: {
      synonyms: ['Companion', 'Pal', 'Buddy'],
      sentences: ['He is my best friend.', 'She made a new friend at school.'],
      definition:
        'A person whom one knows and with whom one has a bond of mutual affection.',
      audioEnglish: 'friend_english.mp3',
      videoEnglish: 'friend_video.mp4',
      popularityIndex: 7,
    },
  },
  {
    word: 'Restaurant',
    translation: 'Ресторан',
    image: 'restaurant.jpg',
    audio: 'restaurant.mp3',
    nextShowDate: '2023-10-05T14:30:00',
    nextReviewLevel: 2,
    associations: {
      image: 'restaurant_association.jpg',
      text: 'Dining place',
    },
    details: {
      synonyms: ['Dining place', 'Eatery', 'Café'],
      sentences: [
        "Let's go to a restaurant for dinner.",
        'This restaurant serves delicious food.',
      ],
      definition:
        'A place where people pay to sit and eat meals that are cooked and served on the premises.',
      audioEnglish: 'restaurant_english.mp3',
      videoEnglish: 'restaurant_video.mp4',
      popularityIndex: 8,
    },
  },
  {
    word: 'Train',
    translation: 'Поезд',
    image: 'train.jpg',
    audio: 'train.mp3',
    nextShowDate: '2023-10-05T11:15:00',
    nextReviewLevel: 1,
    associations: {
      image: 'train_association.jpg',
      text: 'Railway vehicle',
    },
    details: {
      synonyms: ['Railway vehicle', 'Locomotive', 'Express'],
      sentences: [
        'I took the train to work this morning.',
        'The train station is crowded.',
      ],
      definition:
        'A railway vehicle that runs on tracks and is used for transporting passengers or goods.',
      audioEnglish: 'train_english.mp3',
      videoEnglish: 'train_video.mp4',
      popularityIndex: 7,
    },
  },
  {
    word: 'Bicycle',
    translation: 'Велосипед',
    image: 'bicycle.jpg',
    audio: 'bicycle.mp3',
    nextShowDate: '2023-10-20T09:45:00',
    nextReviewLevel: 1,
    associations: {
      image: 'bicycle_association.jpg',
      text: 'Cycle',
    },
    details: {
      synonyms: ['Cycle', 'Bike', 'Two-wheeler'],
      sentences: [
        'I ride my bicycle to work every day.',
        'Learning to ride a bicycle is fun.',
      ],
      definition: 'A vehicle with two wheels that is powered by pedals.',
      audioEnglish: 'bicycle_english.mp3',
      videoEnglish: 'bicycle_video.mp4',
      popularityIndex: 7,
    },
  },
  {
    word: 'Library',
    translation: 'Библиотека',
    image: 'library.jpg',
    audio: 'library.mp3',
    nextShowDate: '2023-10-18T14:20:00',
    nextReviewLevel: 2,
    associations: {
      image: 'library_association.jpg',
      text: 'Books',
    },
    details: {
      synonyms: ['Books', 'Reading room', 'Bookstore'],
      sentences: [
        'I love spending time in the library.',
        'The library has a vast collection of books.',
      ],
      definition:
        'A place where books, magazines, and other materials are available for borrowing or reading.',
      audioEnglish: 'library_english.mp3',
      videoEnglish: 'library_video.mp4',
      popularityIndex: 8,
    },
  },
  {
    word: 'Doctor',
    translation: 'Доктор',
    image: 'doctor.jpg',
    audio: 'doctor.mp3',
    nextShowDate: '2023-10-15T11:30:00',
    nextReviewLevel: 1,
    associations: {
      image: 'doctor_association.jpg',
      text: 'Physician',
    },
    details: {
      synonyms: ['Physician', 'Medic', 'Healthcare provider'],
      sentences: [
        "I need to see the doctor because I'm not feeling well.",
        'The doctor gave me some medicine.',
      ],
      definition:
        'A person qualified to practice medicine, usually a physician or a surgeon.',
      audioEnglish: 'doctor_english.mp3',
      videoEnglish: 'doctor_video.mp4',
      popularityIndex: 9,
    },
  },
  {
    word: 'Beach',
    translation: 'Пляж',
    image: 'beach.jpg',
    audio: 'beach.mp3',
    nextShowDate: '2023-09-25T16:10:00',
    nextReviewLevel: 1,
    associations: {
      image: 'beach_association.jpg',
      text: 'Seashore',
    },
    details: {
      synonyms: ['Seashore', 'Coast', 'Shoreline'],
      sentences: [
        'We spent the day at the beach.',
        'The beach is beautiful at sunset.',
      ],
      definition:
        'A sandy or pebbly area by the sea or a lake, especially one that is used for relaxation or recreation.',
      audioEnglish: 'beach_english.mp3',
      videoEnglish: 'beach_video.mp4',
      popularityIndex: 7,
    },
  },
  {
    word: 'Music',
    translation: 'Музыка',
    image: 'music.jpg',
    audio: 'music.mp3',
    nextShowDate: '2023-09-22T18:30:00',
    nextReviewLevel: 2,
    associations: {
      image: 'music_association.jpg',
      text: 'Sounds',
    },
    details: {
      synonyms: ['Sounds', 'Melodies', 'Tunes'],
      sentences: [
        'I enjoy listening to music.',
        'She plays musical instruments.',
      ],
      definition:
        'Sounds that are deliberately created to be organized and enjoyable to listen to.',
      audioEnglish: 'music_english.mp3',
      videoEnglish: 'music_video.mp4',
      popularityIndex: 8,
    },
  },
  {
    word: 'Computer',
    translation: 'Компьютер',
    image: 'computer.jpg',
    audio: 'computer.mp3',
    nextShowDate: '2023-09-18T14:45:00',
    nextReviewLevel: 1,
    associations: {
      image: 'computer_association.jpg',
      text: 'Machine',
    },
    details: {
      synonyms: ['Machine', 'PC', 'Laptop'],
      sentences: [
        'I use a computer for work.',
        "He's good at programming computers.",
      ],
      definition:
        'An electronic device that can store, retrieve, and process data.',
      audioEnglish: 'computer_english.mp3',
      videoEnglish: 'computer_video.mp4',
      popularityIndex: 9,
    },
  },
  {
    word: 'Dance',
    translation: 'Танцы',
    image: 'dance.jpg',
    audio: 'dance.mp3',
    nextShowDate: '2023-09-12T20:15:00',
    nextReviewLevel: 1,
    associations: {
      image: 'dance_association.jpg',
      text: 'Movement',
    },
    details: {
      synonyms: ['Movement', 'Rhythm', 'Choreography'],
      sentences: [
        'They love to dance at parties.',
        'She took dance lessons as a child.',
      ],
      definition: 'A series of movements that match the rhythm and music.',
      audioEnglish: 'dance_english.mp3',
      videoEnglish: 'dance_video.mp4',
      popularityIndex: 7,
    },
  },
  {
    word: 'Shop',
    translation: 'Магазин',
    image: 'shop.jpg',
    audio: 'shop.mp3',
    nextShowDate: '2023-09-08T09:55:00',
    nextReviewLevel: 1,
    associations: {
      image: 'shop_association.jpg',
      text: 'Store',
    },
    details: {
      synonyms: ['Store', 'Retailer', 'Boutique'],
      sentences: [
        "Let's go shopping for clothes.",
        'The shop is closed on Sundays.',
      ],
      definition: 'A place where goods or services are sold to customers.',
      audioEnglish: 'shop_english.mp3',
      videoEnglish: 'shop_video.mp4',
      popularityIndex: 8,
    },
  },
  {
    word: 'Garden',
    translation: 'Сад',
    image: 'garden.jpg',
    audio: 'garden.mp3',
    nextShowDate: '2023-09-30T11:20:00',
    nextReviewLevel: 1,
    associations: {
      image: 'garden_association.jpg',
      text: 'Backyard',
    },
    details: {
      synonyms: ['Backyard', 'Yard', 'Green space'],
      sentences: [
        'I love spending time in my garden.',
        'She planted flowers in her garden.',
      ],
      definition:
        'An outdoor space used for growing plants, flowers, or vegetables.',
      audioEnglish: 'garden_english.mp3',
      videoEnglish: 'garden_video.mp4',
      popularityIndex: 7,
    },
  },
  {
    word: 'Hobby',
    translation: 'Хобби',
    image: 'hobby.jpg',
    audio: 'hobby.mp3',
    nextShowDate: '2023-09-28T15:10:00',
    nextReviewLevel: 2,
    associations: {
      image: 'hobby_association.jpg',
      text: 'Interest',
    },
    details: {
      synonyms: ['Interest', 'Pastime', 'Leisure activity'],
      sentences: [
        'My hobby is painting.',
        'He has many hobbies, including fishing and hiking.',
      ],
      definition:
        "An activity or interest pursued for pleasure or relaxation during one's free time.",
      audioEnglish: 'hobby_english.mp3',
      videoEnglish: 'hobby_video.mp4',
      popularityIndex: 8,
    },
  },
  {
    word: 'Weather',
    translation: 'Погода',
    image: 'weather.jpg',
    audio: 'weather.mp3',
    nextShowDate: '2023-09-26T17:45:00',
    nextReviewLevel: 1,
    associations: {
      image: 'weather_association.jpg',
      text: 'Climate',
    },
    details: {
      synonyms: ['Climate', 'Conditions', 'Forecast'],
      sentences: [
        'The weather today is sunny and warm.',
        'They checked the weather forecast before their trip.',
      ],
      definition:
        'The state of the atmosphere at a particular place and time, as described by various meteorological parameters.',
      audioEnglish: 'weather_english.mp3',
      videoEnglish: 'weather_video.mp4',
      popularityIndex: 9,
    },
  },
  {
    word: 'Family',
    translation: 'Семья',
    image: 'family.jpg',
    audio: 'family.mp3',
    nextShowDate: '2023-09-24T10:30:00',
    nextReviewLevel: 1,
    associations: {
      image: 'family_association.jpg',
      text: 'Relatives',
    },
    details: {
      synonyms: ['Relatives', 'Kin', 'Clan'],
      sentences: [
        'I spend time with my family on weekends.',
        'Her family is very supportive.',
      ],
      definition:
        'A group of people related by blood, marriage, or adoption, living together or connected by common ancestry.',
      audioEnglish: 'family_english.mp3',
      videoEnglish: 'family_video.mp4',
      popularityIndex: 7,
    },
  },
  {
    word: 'Travel',
    translation: 'Путешествие',
    image: 'travel.jpg',
    audio: 'travel.mp3',
    nextShowDate: '2023-09-22T14:55:00',
    nextReviewLevel: 1,
    associations: {
      image: 'travel_association.jpg',
      text: 'Journey',
    },
    details: {
      synonyms: ['Journey', 'Trip', 'Adventure'],
      sentences: [
        'I love to travel and explore new places.',
        "They're planning a travel adventure to Europe.",
      ],
      definition:
        'The act of going from one place to another, especially over a long distance.',
      audioEnglish: 'travel_english.mp3',
      videoEnglish: 'travel_video.mp4',
      popularityIndex: 8,
    },
  },
  {
    word: 'Park',
    translation: 'Парк',
    image: 'park.jpg',
    audio: 'park.mp3',
    nextShowDate: '2023-09-20T09:15:00',
    nextReviewLevel: 1,
    associations: {
      image: 'park_association.jpg',
      text: 'Green space',
    },
    details: {
      synonyms: ['Green space', 'Recreation area', 'Garden'],
      sentences: [
        "Let's have a picnic in the park.",
        'The park is a great place for outdoor activities.',
      ],
      definition:
        'An area of natural land, often in a city, set aside for public enjoyment and recreation.',
      audioEnglish: 'park_english.mp3',
      videoEnglish: 'park_video.mp4',
      popularityIndex: 7,
    },
  },
  {
    word: 'Phone',
    translation: 'Телефон',
    image: 'phone.jpg',
    audio: 'phone.mp3',
    nextShowDate: '2023-09-18T15:50:00',
    nextReviewLevel: 2,
    associations: {
      image: 'phone_association.jpg',
      text: 'Mobile device',
    },
    details: {
      synonyms: ['Mobile device', 'Cellphone', 'Smartphone'],
      sentences: [
        'I always carry my phone with me.',
        'She sent a text message from her phone.',
      ],
      definition:
        'A portable electronic device used for making telephone calls and sending text messages.',
      audioEnglish: 'phone_english.mp3',
      videoEnglish: 'phone_video.mp4',
      popularityIndex: 8,
    },
  },
  {
    word: 'Book',
    translation: 'Книга',
    image: 'book.jpg',
    audio: 'book.mp3',
    nextShowDate: '2023-09-16T11:30:00',
    nextReviewLevel: 1,
    associations: {
      image: 'book_association.jpg',
      text: 'Reading material',
    },
    details: {
      synonyms: ['Reading material', 'Novel', 'Publication'],
      sentences: [
        'I enjoy reading books in my free time.',
        'She borrowed a book from the library.',
      ],
      definition:
        'A written or printed work consisting of pages glued or sewn together along one side and bound in covers.',
      audioEnglish: 'book_english.mp3',
      videoEnglish: 'book_video.mp4',
      popularityIndex: 9,
    },
  },
  {
    word: 'Food',
    translation: 'Еда',
    image: 'food.jpg',
    audio: 'food.mp3',
    nextShowDate: '2023-09-14T17:15:00',
    nextReviewLevel: 1,
    associations: {
      image: 'food_association.jpg',
      text: 'Nourishment',
    },
    details: {
      synonyms: ['Nourishment', 'Cuisine', 'Dish'],
      sentences: [
        'I love trying different types of food.',
        'The restaurant serves delicious food.',
      ],
      definition:
        'Any nutritious substance that people or animals eat or drink to maintain life and growth.',
      audioEnglish: 'food_english.mp3',
      videoEnglish: 'food_video.mp4',
      popularityIndex: 8,
    },
  },
  {
    word: 'Movie',
    translation: 'Кино',
    image: 'movie.jpg',
    audio: 'movie.mp3',
    nextShowDate: '2023-09-12T14:00:00',
    nextReviewLevel: 1,
    associations: {
      image: 'movie_association.jpg',
      text: 'Film',
    },
    details: {
      synonyms: ['Film', 'Cinema', 'Motion picture'],
      sentences: [
        "Let's go watch a movie at the theater.",
        'She enjoys watching action movies.',
      ],
      definition:
        'A story or event recorded by a camera as a set of moving images and shown in a theater or on television.',
      audioEnglish: 'movie_english.mp3',
      videoEnglish: 'movie_video.mp4',
      popularityIndex: 7,
    },
  },
];
