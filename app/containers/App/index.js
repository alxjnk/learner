/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { useEffect } from 'react';
import { I18nextProvider } from 'react-i18next';
import Router from '../Router';
import GlobalStyle from '../../global-styles';
import i18n from '../translations/i18n';

function App() {
  useEffect(() => {
    document.body.classList.add('Dark');
  }, []);

  return (
    <I18nextProvider i18n={i18n}>
      <Router />
      <GlobalStyle />
    </I18nextProvider>
  );
}

export default App;
