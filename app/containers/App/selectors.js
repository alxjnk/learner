import { createSelector } from 'reselect';
import { differenceInMilliseconds, isBefore } from 'date-fns';
import {
  ChartBar,
  List,
  ShoppingCart,
  ToolsKitchen2,
  Users,
  Bell,
  BuildingStore,
  ClipboardList,
} from 'tabler-icons-react';
import { getIsNotificationListEmptySelector } from '../NotificationPage/selectors';
import { getIsNextDayPlanWasSetSelector } from '../Stats/selectors';

const selectRouter = state => state.router;
const selectAuth = state => state.auth;

const makeSelectLocation = () =>
  createSelector(
    selectRouter,
    routerState => routerState.location,
  );

const getTimeBeforeTokenExpiresSelector = () =>
  createSelector(
    selectAuth,
    ({ tokens }) => {
      if (!tokens) return -1;
      const different = differenceInMilliseconds(
        tokens.access.expires,
        new Date(),
      );
      return different - 10000;
    },
  );

const getIsTokenExpiresSelector = () =>
  createSelector(
    selectAuth,
    ({ tokens }) => {
      if (!tokens) return false;
      return isBefore(tokens.access.expires, new Date());
    },
  );

const getIsUserAuthorizedSelector = () =>
  createSelector(
    selectAuth,
    ({ isAuthorized }) => isAuthorized,
  );

// TABS START

const getUserTabsSelector = () =>
  createSelector(
    getIsNotificationListEmptySelector(),
    isNotificationListEmpty => [
      { name: 'tasks', icon: List },
      { name: 'stocks', icon: ShoppingCart },
      { name: 'menu', icon: ClipboardList },
      { name: 'notifications', icon: Bell, notice: !isNotificationListEmpty },
    ],
  );

const getChefTabsSelector = () =>
  createSelector(
    getIsNotificationListEmptySelector(),
    getIsNextDayPlanWasSetSelector(),
    (isNotificationListEmpty, isNextDayPlanWasSet) => [
      { name: 'stats', icon: ChartBar, notice: !isNextDayPlanWasSet },
      { name: 'cooks', icon: Users },
      { name: 'sections-tasks', icon: ToolsKitchen2 },
      { name: 'menu', icon: ClipboardList },
      { name: 'notifications', icon: Bell, notice: !isNotificationListEmpty },
    ],
  );

const getStoreTabsSelector = () =>
  createSelector(
    getIsNotificationListEmptySelector(),
    isNotificationListEmpty => [
      { name: 'store', icon: BuildingStore },
      { name: 'notifications', icon: Bell, notice: !isNotificationListEmpty },
    ],
  );

const getAnyTypeUserTabsSelector = () =>
  createSelector(
    selectAuth,
    getUserTabsSelector(),
    getChefTabsSelector(),
    getStoreTabsSelector(),
    ({ user }, userTabs, chefTabs, storeTabs) => {
      switch (user.role) {
        case 'admin' || 'chef':
          return chefTabs;
        case 'store':
          return storeTabs;
        default:
          return userTabs;
      }
    },
  );

// TABS END

const getIsUserChefAndPlanWasNotSetSelector = () =>
  createSelector(
    getIsNextDayPlanWasSetSelector(),
    selectAuth,
    (isNextDayPlanWasSet, { user }) =>
      !isNextDayPlanWasSet && user?.role === 'admin',
  );

const getTokenExpiresTimeSelector = () =>
  createSelector(
    selectAuth,
    ({ tokens }) => {
      if (!tokens) return null;
      return tokens.access.expires;
    },
  );

export {
  makeSelectLocation,
  getTimeBeforeTokenExpiresSelector,
  getIsTokenExpiresSelector,
  getIsUserAuthorizedSelector,
  getUserTabsSelector,
  getChefTabsSelector,
  getStoreTabsSelector,
  getAnyTypeUserTabsSelector,
  getIsUserChefAndPlanWasNotSetSelector,
  getTokenExpiresTimeSelector,
};
