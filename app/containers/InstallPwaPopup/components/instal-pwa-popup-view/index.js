import React from 'react';
import { Button } from 'semantic-ui-react';
import './styles.less';
import PropTypes from 'prop-types';
import { ModalWindow } from '../../../../my-custom-semantic-theme/components/modal-window';
import shareButton from '../../../../images/share-icon.svg';

const InstallPwaPopupView = ({
  onInstallButtonClick,
  buttonText,
  isModalShown,
  onModalClose,
  modalCloseButtonText,
  pressShareText,
  addToHomeScreenText,
}) => (
  <div className="install-pwa-popup">
    {isModalShown && (
      <ModalWindow
        isModalShown={isModalShown}
        onClose={onModalClose}
        headerText={buttonText}
        actionButtons={[
          {
            color: 'secondary',
            onClick: onModalClose,
            text: modalCloseButtonText,
          },
        ]}
        modalContent={
          <div className="install-pwa-popup__modal-content">
            <div className="install-pwa-popup__modal-content-item">
              <span>{pressShareText}</span>
              <img src={shareButton} alt="share button" />
            </div>
            <div className="install-pwa-popup__modal-content-item">
              <span>{addToHomeScreenText}</span>
            </div>
          </div>
        }
      />
    )}
    <div className="install-pwa-popup__button">
      <Button
        secondary
        size="small"
        className="link-button"
        aria-label="Install app"
        onClick={onInstallButtonClick}
      >
        {buttonText}
      </Button>
    </div>
  </div>
);

InstallPwaPopupView.propTypes = {
  isModalShown: PropTypes.bool.isRequired,
  onInstallButtonClick: PropTypes.func.isRequired,
  buttonText: PropTypes.string.isRequired,
  modalCloseButtonText: PropTypes.string.isRequired,
  onModalClose: PropTypes.func.isRequired,
  pressShareText: PropTypes.string.isRequired,
  addToHomeScreenText: PropTypes.string.isRequired,
};

export { InstallPwaPopupView };
