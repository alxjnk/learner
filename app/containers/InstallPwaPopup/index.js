import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import './components/instal-pwa-popup-view/styles.less';
import { InstallPwaPopupView } from './components/instal-pwa-popup-view';

const checkIsIos = () => {
  const ua = window.navigator.userAgent;
  const isIPad = Boolean(ua.match(/iPad/i));
  const isIPhone = Boolean(ua.match(/iPhone/i));
  return isIPad || isIPhone;
};

const checkIsInStandaloneMode = () =>
  'standalone' in window.navigator && window.navigator.standalone;

const InstallPwaPopup = () => {
  const [isSupportsPWA, setIsSupportsPWA] = useState(false);
  const [promptInstall, setPromptInstall] = useState(null);
  const [isIos, setIsIos] = useState(true);
  const [isModalShown, setIsModalShown] = useState(false);

  const { t } = useTranslation();

  useEffect(() => {
    const handler = e => {
      e.preventDefault();
      setIsSupportsPWA(true);
      setPromptInstall(e);
    };

    setIsIos(checkIsIos() && !checkIsInStandaloneMode());

    window.addEventListener('beforeinstallprompt', handler);

    return () => window.removeEventListener('transitionend', handler);
  }, []);

  const handleInstallButtonClick = e => {
    e.preventDefault();

    if (isIos) {
      setIsModalShown(true);
    }

    if (!promptInstall) {
      return;
    }
    promptInstall.prompt();
  };

  const handleModalClose = () => setIsModalShown(false);

  return isIos || (!isIos && isSupportsPWA) ? (
    <InstallPwaPopupView
      onInstallButtonClick={handleInstallButtonClick}
      buttonText={t('install')}
      isModalShown={isModalShown}
      onModalClose={handleModalClose}
      modalCloseButtonText={t('close')}
      pressShareText={t('press share')}
      addToHomeScreenText={t('add to home screen')}
    />
  ) : null;
};

export default InstallPwaPopup;
