export const getNotArchiveReceipts = receiptList =>
  receiptList.filter(receipt => !receipt.isArchived);
