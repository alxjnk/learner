export const GET_SECTIONS = 'app/BooksPage/GET_SECTIONS';
export function getSectionsAction() {
  return {
    type: GET_SECTIONS,
  };
}

export const SET_SECTIONS = 'app/BooksPage/SET_SECTIONS';
export function setSectionsAction(payload) {
  return {
    type: SET_SECTIONS,
    payload,
  };
}

export const SET_USERS = 'app/BooksPage/SET_USERS';
export function setUsersAction(payload) {
  return {
    type: SET_USERS,
    payload,
  };
}

export const SET_RECEIPTS = 'app/BooksPage/SET_RECEIPTS';
export function setReceiptsAction(payload) {
  return {
    type: SET_RECEIPTS,
    payload,
  };
}

export const SET_INGREDIENTS = 'app/BooksPage/SET_INGREDIENTS';
export function setIngredientsAction(payload) {
  return {
    type: SET_INGREDIENTS,
    payload,
  };
}

export const SET_CHECKLISTS = 'app/BooksPage/SET_CHECKLISTS';
export function setChecklistsAction(payload) {
  return {
    type: SET_CHECKLISTS,
    payload,
  };
}

export const UPDATE_BOOK_AT_STORE = 'app/BooksPage/UPDATE_BOOK_AT_STORE';
export function updateBookAtStore(payload) {
  return { type: UPDATE_BOOK_AT_STORE, payload };
}

export const DELETE_BOOK_AT_STORE = 'app/BooksPage/DELETE_BOOK_AT_STORE';
export function deleteBookAtStore(payload) {
  return { type: DELETE_BOOK_AT_STORE, payload };
}

export const GET_USERS = 'app/BooksPage/GET_USERS';
export function getUsersAction() {
  return {
    type: GET_USERS,
  };
}
