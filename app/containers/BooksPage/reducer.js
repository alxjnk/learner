import { AUTH_LOGOUT_COMPLETE } from '../../modules/auth/actionTypes';
import {
  SET_SECTIONS,
  SET_USERS,
  SET_RECEIPTS,
  UPDATE_BOOK_AT_STORE,
  DELETE_BOOK_AT_STORE,
  GET_SECTIONS,
  SET_INGREDIENTS,
  SET_CHECKLISTS,
} from './actions';

export const initialState = {
  sections: {},
  users: {},
  receipts: {},
  ingredients: {},
  checkLists: {},
  isLoading: false,
};

const booksPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_LOGOUT_COMPLETE:
      return initialState;
    case GET_SECTIONS:
      return { ...state, isLoading: true };
    case SET_USERS:
      return { ...state, users: action.payload, isLoading: false };
    case SET_RECEIPTS:
      return { ...state, receipts: action.payload, isLoading: false };
    case SET_SECTIONS:
      return { ...state, sections: action.payload, isLoading: false };
    case SET_INGREDIENTS:
      return { ...state, ingredients: action.payload, isLoading: false };
    case SET_CHECKLISTS:
      return { ...state, checkLists: action.payload, isLoading: false };
    case UPDATE_BOOK_AT_STORE:
      return {
        ...state,
        [action.payload.book]: {
          ...state[action.payload.book],
          [action.payload.id]: action.payload.data,
        },
        isLoading: false,
      };
    case DELETE_BOOK_AT_STORE:
      const newState = state;
      delete newState[action.payload.book][action.payload.id];
      return { ...newState, isLoading: false };
    default:
      return state;
  }
};

export default booksPageReducer;
