import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectBooksPageDomain = state => state.booksPage || initialState;

const makeSelectBooksPage = () =>
  createSelector(
    selectBooksPageDomain,
    substate => substate,
  );

const getSectionsSelector = () =>
  createSelector(
    selectBooksPageDomain,
    ({ sections }) => sections,
  );

const getUsersSelector = () =>
  createSelector(
    selectBooksPageDomain,
    ({ users }) => users,
  );

const getReceiptsSelector = () =>
  createSelector(
    selectBooksPageDomain,
    ({ receipts }) => receipts,
  );

const getIngredientsSelector = () =>
  createSelector(
    selectBooksPageDomain,
    ({ ingredients }) => ingredients,
  );

const getCheckListsSelector = () =>
  createSelector(
    selectBooksPageDomain,
    ({ checkLists }) => checkLists,
  );

const getIsBooksLoadingSelector = () =>
  createSelector(
    selectBooksPageDomain,
    ({ isLoading }) => isLoading,
  );

export default makeSelectBooksPage;
export {
  getSectionsSelector,
  getUsersSelector,
  getReceiptsSelector,
  getIsBooksLoadingSelector,
  getIngredientsSelector,
  getCheckListsSelector,
};
