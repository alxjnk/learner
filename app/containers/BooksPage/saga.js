import { keyBy } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { call, put, select, takeEvery, all, take } from 'redux-saga/effects';
import { notificationInit } from '../../modules/notifications/actions';
import { getTokens } from '../../modules/auth/selectors';
import {
  getCheckListsRequest,
  getIngredientsRequest,
  getReceiptsListRequest,
  getSectionsRequest,
  getUsersListRequest,
} from '../../utils/api/requests';
import {
  GET_SECTIONS,
  setSectionsAction,
  setReceiptsAction,
  setUsersAction,
  GET_USERS,
  setIngredientsAction,
  setChecklistsAction,
} from './actions';
import { authRefreshTokenRequest } from '../../modules/auth/actions';
import { AUTH_REFRESH_TOKEN } from '../../modules/auth/actionTypes';
import { getIsTokenExpiresSelector } from '../App/selectors';
import { getNotArchiveReceipts } from './utils';

function* getBooksSaga() {
  try {
    const { access } = yield select(getTokens);
    const [sections, receipts, ingredients, checkLists] = yield all([
      call(getSectionsRequest, access.token),
      call(getReceiptsListRequest, access.token),
      call(getIngredientsRequest, access.token),
      call(getCheckListsRequest, access.token),
    ]);
    yield put(setSectionsAction(keyBy(sections, 'id')));
    yield put(setReceiptsAction(keyBy(getNotArchiveReceipts(receipts), 'id')));
    yield put(setIngredientsAction(keyBy(ingredients, 'id')));
    yield put(setChecklistsAction(keyBy(checkLists, 'id')));
  } catch (error) {
    yield put(
      notificationInit({
        id: uuidv4(),
        message: error.message,
        type: 'warning',
        dismissAfter: 3000,
      }),
    );
  }
}

function* getUsersSaga() {
  try {
    const isTokenExpired = yield select(getIsTokenExpiresSelector());
    if (isTokenExpired) {
      yield put(authRefreshTokenRequest());
      yield take(AUTH_REFRESH_TOKEN);
    }
    const { access } = yield select(getTokens);
    const [users] = yield all([call(getUsersListRequest, access.token)]);
    yield put(setUsersAction(keyBy(users, 'id')));
  } catch (error) {
    if (error.message === 'Please authenticate') {
      yield put(authRefreshTokenRequest({ data: 'userRequest' }));
    } else {
      yield put(
        notificationInit({
          id: uuidv4(),
          message: error.message,
          type: 'warning',
          dismissAfter: 3000,
        }),
      );
    }
  }
}

export default function* booksPageSaga() {
  yield takeEvery(GET_SECTIONS, getBooksSaga);
  yield takeEvery(GET_USERS, getUsersSaga);
}
