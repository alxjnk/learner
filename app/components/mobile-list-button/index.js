import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import './styles.less';

const CLASS_NAME = 'mobile-list-button';

function MobileListButton({ onBtnClick, text, color }) {
  const { t } = useTranslation();
  return (
    <button
      className={`${CLASS_NAME} ${color ? `${CLASS_NAME}_${color}` : ''}`}
      type="button"
      onClick={onBtnClick}
    >
      {t(text)}
    </button>
  );
}

MobileListButton.propTypes = {
  onBtnClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  color: PropTypes.string,
};

export { MobileListButton };
