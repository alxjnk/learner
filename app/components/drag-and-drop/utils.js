export const currentItemPosition = (
  order,
  itemHeight,
  down,
  originalIndex,
  curIndex,
  y
) => (index) => {
  return down && index === originalIndex
    ? {
        y: curIndex * itemHeight + y,
        scale: 1.1,
        zIndex: "1",
        shadow: 15,
        immediate: (n) => n === "y" || n === "zIndex",
      }
    : {
        y: order.indexOf(index) * itemHeight,
        scale: 1,
        zIndex: "0",
        shadow: 1,
        immediate: false,
      };
};
