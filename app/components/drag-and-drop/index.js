/* eslint-disable react/no-children-prop */
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import clamp from 'lodash-es/clamp';
import swap from 'lodash-move';
import { useGesture } from 'react-use-gesture';
import { useSprings, animated, interpolate } from 'react-spring';

import { currentItemPosition } from './utils';

import './styles.less';

function DragAndDrop({
  data,
  itemHeight,
  getOrderData,
  setOrderData,
  component: Item,
}) {
  const orderData = getOrderData && getOrderData(data.length);

  const order = useRef(orderData || data.map((_, index) => index)); // Store indicies as a local ref, this represents the item order

  const [springs, setSprings] = useSprings(
    data.length,
    currentItemPosition(order.current, itemHeight),
  ); // Create springs, each corresponds to an item, controlling its transform, scale, etc.

  const bind = useGesture(({ args: [originalIndex], down, delta: [, y] }) => {
    const curIndex = order.current.indexOf(originalIndex);
    const curRow = clamp(
      Math.round((curIndex * 100 + y) / 100),
      0,
      data.length - 1,
    );
    const newOrder = swap(order.current, curIndex, curRow);

    setSprings(
      currentItemPosition(
        newOrder,
        itemHeight,
        down,
        originalIndex,
        curIndex,
        y,
      ),
    ); // Feed springs new style data, they'll animate the view without causing a single render

    if (!down) order.current = newOrder;
    if (setOrderData) {
      setOrderData(newOrder);
    }
  });

  const eventStopPropagation = e => {
    e.stopPropagation();
  };

  return (
    <>
      {springs.map(({ zIndex, y, scale }, i) => (
        <animated.div
          {...bind(i)}
          key={i.toString()}
          style={{
            zIndex,
            transform: interpolate(
              [y, scale],
              (y, s) => `translate3d(0,${y}px,0) scale(${s})`,
            ),
          }}
          className="drag-and-drop-item"
          children={
            <Item
              el={data[i]}
              key={data[i].name}
              eventStopPropagation={eventStopPropagation}
            />
          }
        />
      ))}
    </>
  );
}

DragAndDrop.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  itemHeight: PropTypes.number,
  getOrderData: PropTypes.func,
  setOrderData: PropTypes.func,
  component: PropTypes.elementType,
};

export { DragAndDrop };
