import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';

function ColoredButton({ color, text, onClick }) {
  return (
    <Button color={color} onClick={onClick} size="large">
      {text}
    </Button>
  );
}

ColoredButton.propTypes = {
  color: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export { ColoredButton };
