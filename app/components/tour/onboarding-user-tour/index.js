import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  setOnboardingUserTourShownAction,
  setOnboardingUserTourStepAction,
} from '../../../modules/tour/actions';
import {
  getIsOnboardingUserTourShownSelector,
  getOnboardingUserTourStepSelector,
} from '../../../modules/tour/selectors';
import { CustomTour } from '../custom-tour';
import history from '../../../utils/history';
import { onboardingUserTourSteps } from './constants';

function OnboardingUserTourComponent({
  isTourShown,
  onSetTourShown,
  step,
  onSetTourStep,
}) {
  useEffect(() => {
    history.push('/tasks');
  }, []);

  const handleCloseTour = () => {
    onSetTourStep(0);
    onSetTourShown(false);
  };

  const handleChangeTourStep = currStep => {
    onSetTourStep(currStep);
  };

  return (
    <CustomTour
      tourSteps={onboardingUserTourSteps}
      isTourShown={isTourShown}
      onCloseTour={handleCloseTour}
      onChangeTourStep={handleChangeTourStep}
      step={step}
    />
  );
}

OnboardingUserTourComponent.propTypes = {
  isTourShown: PropTypes.bool.isRequired,
  onSetTourShown: PropTypes.func.isRequired,
  step: PropTypes.number,
  onSetTourStep: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isTourShown: getIsOnboardingUserTourShownSelector(),
  step: getOnboardingUserTourStepSelector(),
});

const mapDispatchToProps = {
  onSetTourShown: setOnboardingUserTourShownAction,
  onSetTourStep: setOnboardingUserTourStepAction,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const OnboardingUserTour = compose(withConnect)(OnboardingUserTourComponent);

export { OnboardingUserTour };
