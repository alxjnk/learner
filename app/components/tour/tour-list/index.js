import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { OnboardingUserTour } from '../onboarding-user-tour';
import {
  isTourWasShownBefore,
  setTourWasShownBefore,
} from '../../../modules/tour/constants';
import { getIsUserAdmin } from '../../../modules/auth/selectors';
import { setOnboardingUserTourShownAction } from '../../../modules/tour/actions';
import { getIsOneOfTourShownSelector } from '../../../modules/tour/selectors';

function TourListComponent({
  isUserAdmin,
  onSetOnboardingUserTourShown,
  isOneOfTourShown,
}) {
  useEffect(() => {
    if (!isTourWasShownBefore()) {
      if (!isUserAdmin) {
        setTourWasShownBefore();
        onSetOnboardingUserTourShown(true);
      }
    }
  }, []);
  if (!isOneOfTourShown) {
    return '';
  }
  if (isUserAdmin) {
    return <></>;
  }

  return (
    <>
      <OnboardingUserTour />
    </>
  );
}

TourListComponent.propTypes = {
  isUserAdmin: PropTypes.bool.isRequired,
  onSetOnboardingUserTourShown: PropTypes.func.isRequired,
  isOneOfTourShown: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isUserAdmin: state => getIsUserAdmin(state),
  isOneOfTourShown: getIsOneOfTourShownSelector(),
});

const mapDispatchToProps = {
  onSetOnboardingUserTourShown: setOnboardingUserTourShownAction,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const TourList = compose(withConnect)(TourListComponent);

export { TourList };
