import React from 'react';
import PropTypes from 'prop-types';
import Tour from 'reactour';

function CustomTour({
  tourSteps,
  isTourShown,
  onCloseTour,
  onChangeTourStep,
  step,
}) {
  return (
    <Tour
      rounded={6}
      disableFocusLock
      className="tour"
      steps={tourSteps}
      isOpen={isTourShown}
      onRequestClose={onCloseTour}
      getCurrentStep={onChangeTourStep}
      goToStep={step}
    />
  );
}
CustomTour.propTypes = {
  tourSteps: PropTypes.arrayOf(PropTypes.object).isRequired,
  isTourShown: PropTypes.bool.isRequired,
  onCloseTour: PropTypes.func.isRequired,
  onChangeTourStep: PropTypes.func.isRequired,
  step: PropTypes.number,
};

export { CustomTour };
