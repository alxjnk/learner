import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import './styles.less';

function BlurPageWithDateInput({
  selectedDate,
  onChangeDate,
  onClose,
  eventStopPropagation,
  markedDate,
  minDate,
  component: PickerComponent,
  firstDayOfSelectedMonth,
  onSetFirstDayOfSelectedMonth,
}) {
  const { i18n } = useTranslation();

  return (
    <div
      onClick={onClose}
      className="blur-page-with-date-input"
      role="button"
      tabIndex={0}
    >
      <div
        onClick={eventStopPropagation}
        className="blur-page-with-date-input__wrapper"
        role="button"
        tabIndex={0}
      >
        <PickerComponent
          selectedDate={selectedDate}
          onChangeDate={onChangeDate}
          markedDate={markedDate}
          minDate={minDate}
          localization={i18n.language}
          isInline
          firstDayOfViewMonth={firstDayOfSelectedMonth}
          onSetFirstDayOfViewMonth={onSetFirstDayOfSelectedMonth}
        />
      </div>
    </div>
  );
}

BlurPageWithDateInput.propTypes = {
  onChangeDate: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  selectedDate: PropTypes.string.isRequired,
  eventStopPropagation: PropTypes.func.isRequired,
  markedDate: PropTypes.arrayOf(PropTypes.instanceOf(Date)),
  minDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  component: PropTypes.elementType.isRequired,
  onSetFirstDayOfSelectedMonth: PropTypes.func,
  firstDayOfSelectedMonth: PropTypes.string,
};

export { BlurPageWithDateInput };
