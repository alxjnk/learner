import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { ModalWindow } from '../../my-custom-semantic-theme/components/modal-window';

function UpdateAppModal({ isReceiveNewSW, setIsReceiveNewSW }) {
  const { t } = useTranslation();

  const handleCloseModal = () => {
    setIsReceiveNewSW(false);
  };

  const handleRefreshPage = () => {
    window.location.reload();
  };

  return (
    <ModalWindow
      isModalShown={isReceiveNewSW}
      onClose={handleCloseModal}
      headerText={t('Notification')}
      modalContent={t(
        'There is a new version of the application, need to restart the app',
      )}
      actionButtons={[
        {
          color: 'primary',
          text: t('Refresh'),
          onClick: handleRefreshPage,
        },
        { color: 'secondary', text: t('Cancel'), onClick: handleCloseModal },
      ]}
    />
  );
}

UpdateAppModal.propTypes = {
  isReceiveNewSW: PropTypes.bool.isRequired,
  setIsReceiveNewSW: PropTypes.func.isRequired,
};

export { UpdateAppModal };
