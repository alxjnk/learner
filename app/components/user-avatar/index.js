/*
 *
 * Avatar component
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';
import './index.less';

const UserAvatar = ({ name, img }) => (
  <div className="user-avatar">
    {img && <Image src={img} circular />}
    {!img && <div className="user-avatar__text-wrapper">{name}</div>}
  </div>
);

UserAvatar.propTypes = {
  name: PropTypes.string,
  img: PropTypes.string,
};

export default UserAvatar;
