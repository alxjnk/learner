/*
 *
 * Header component
 *
 */

import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { ChevronLeft } from 'tabler-icons-react';
import HeaderMenu from './header-menu';
import { KitchenIcon } from '../kitchen-icon';
import './styles.less';

export function Header({ title, onBackClick }) {
  useEffect(() => {
    document.body.classList.add('Dark');
  }, []);

  const backButton = useMemo(
    () =>
      onBackClick ? (
        <ChevronLeft size="35" onClick={onBackClick} />
      ) : (
        <KitchenIcon />
      ),
    onBackClick,
  );

  return (
    <header className="header">
      <div className="header-wrapper">
        <div className="header-logo">{backButton}</div>
        <div className="header-title">{title}</div>
        <div className="header-menu" tour-data="header-menu">
          <HeaderMenu />
        </div>
      </div>
    </header>
  );
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  onBackClick: PropTypes.func,
};
