/*
 *
 * Header-Menu-Items-View component
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Menu as SemanticMenu } from 'semantic-ui-react';
import {
  Logout,
  User,
  // Language,
  // ClipboardList,
  // Book,
} from 'tabler-icons-react';
import { useTranslation } from 'react-i18next';
import './styles.less';

export default function HeaderMenuItemsView({
  onLogoutClick,
  onOpenProfilePage,
  // onChangeLanguage,
  // onClickUserSchedule,
  // onStartTour,
  // isTourItemShown,
}) {
  const { t } = useTranslation();

  return (
    <SemanticMenu vertical tour-data="header-menu__list">
      <SemanticMenu.Item onClick={onOpenProfilePage}>
        <User size="18" className="header-menu__item-icon" />
        {t('profile')}
      </SemanticMenu.Item>
      {/* <SemanticMenu.Item onClick={onChangeLanguage}>
        <Language size="18" className="header-menu__item-icon" />
        {t('nextLanguage')}
      </SemanticMenu.Item>
      <SemanticMenu.Item onClick={onClickUserSchedule}>
        <ClipboardList size="18" className="header-menu__item-icon" />
        {t('UserScheduleMenu')}
      </SemanticMenu.Item>
      {isTourItemShown && (
        <SemanticMenu.Item onClick={onStartTour}>
          <Book size="18" className="header-menu__item-icon" />
          {t('Tour')}
        </SemanticMenu.Item>
      )} */}
      <SemanticMenu.Item onClick={onLogoutClick}>
        <Logout size="18" className="header-menu__item-icon" />
        {t('logOut')}
      </SemanticMenu.Item>
    </SemanticMenu>
  );
}
HeaderMenuItemsView.propTypes = {
  onLogoutClick: PropTypes.func.isRequired,
  onOpenProfilePage: PropTypes.func.isRequired,
};
