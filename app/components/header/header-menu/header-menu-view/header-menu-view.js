/*
 *
 * Header-Menu-View component
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Menu, X } from 'tabler-icons-react';
import HeaderMenuItemsView from '../header-menu-items-view';
import './styles.less';

export default function HeaderMenuView({
  isMenuShow,
  showMenu,
  onLogoutClick,
  onOpenProfilePage,
  onChangeLanguage,
  onClickUserSchedule,
  isTourItemShown,
  onStartTour,
}) {
  HeaderMenuView.propTypes = {
    isMenuShow: PropTypes.bool.isRequired,
    showMenu: PropTypes.func.isRequired,
    onLogoutClick: PropTypes.func.isRequired,
    onOpenProfilePage: PropTypes.func.isRequired,
    onChangeLanguage: PropTypes.func.isRequired,
    onClickUserSchedule: PropTypes.func.isRequired,
    onStartTour: PropTypes.func.isRequired,
    isTourItemShown: PropTypes.bool.isRequired,
  };

  return (
    <div className="header-menu">
      {isMenuShow ? (
        <>
          <X
            size="24"
            onClick={showMenu}
            className="header-menu__trigger-icon"
          />
          <HeaderMenuItemsView
            onLogoutClick={onLogoutClick}
            onOpenProfilePage={onOpenProfilePage}
            onChangeLanguage={onChangeLanguage}
            onClickUserSchedule={onClickUserSchedule}
            isTourItemShown={isTourItemShown}
            onStartTour={onStartTour}
          />
        </>
      ) : (
        <Menu
          size="24"
          onClick={showMenu}
          className="header-menu__trigger-icon"
        />
      )}
    </div>
  );
}
