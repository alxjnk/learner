/*
 *
 * Header-Menu component
 *
 */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { createStructuredSelector } from 'reselect';
import HeaderMenuView from './header-menu-view';
import history from '../../../utils/history';
import { authSetLogoutModalShown } from '../../../modules/auth/actions';
import './styles.less';
import {
  setOnboardingUserTourShownAction,
  setOnboardingUserTourStepAction,
} from '../../../modules/tour/actions';
import { getOnboardingUserTourStepSelector } from '../../../modules/tour/selectors';
import { getIsUserAdmin } from '../../../modules/auth/selectors';

function HeaderMenu({
  onSetLogoutModalShown,
  onboardingUserTourStep,
  onSetOnboardingUserTourStepAction,
  onSetOnboardingUserTourShown,
  isUserAdmin,
}) {
  const { i18n } = useTranslation();

  const [isMenuShow, setIsMenuShow] = useState(false);
  const showMenu = () => {
    if (onboardingUserTourStep === 5) {
      onSetOnboardingUserTourStepAction(onboardingUserTourStep + 1);
    }
    setIsMenuShow(!isMenuShow);
  };

  const handleChangeLanguage = () => {
    if (i18n.language === 'en') {
      i18n.changeLanguage('ru');
    } else {
      i18n.changeLanguage('en');
    }
  };

  const handleLogoutClick = () => {
    onSetLogoutModalShown(true);
  };

  const handleOpenProfilePage = () => {
    history.push('/profile');
  };

  const handleOpenUserSchedulePage = () => {
    history.push('/user-schedule');
    if (onboardingUserTourStep === 6) {
      onSetOnboardingUserTourStepAction(onboardingUserTourStep + 1);
    }
  };

  const handleStartTourPage = () => {
    onSetOnboardingUserTourShown(true);
    setIsMenuShow(false);
  };

  return (
    <HeaderMenuView
      isMenuShow={isMenuShow}
      showMenu={showMenu}
      onLogoutClick={handleLogoutClick}
      onOpenProfilePage={handleOpenProfilePage}
      onChangeLanguage={handleChangeLanguage}
      onClickUserSchedule={handleOpenUserSchedulePage}
      onStartTour={handleStartTourPage}
      isTourItemShown={!isUserAdmin}
    />
  );
}

HeaderMenu.propTypes = {
  onSetLogoutModalShown: PropTypes.func.isRequired,
  onSetOnboardingUserTourStepAction: PropTypes.func.isRequired,
  onboardingUserTourStep: PropTypes.number.isRequired,
  onSetOnboardingUserTourShown: PropTypes.func.isRequired,
  isUserAdmin: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  onboardingUserTourStep: getOnboardingUserTourStepSelector(),
  isUserAdmin: state => getIsUserAdmin(state),
});

const mapDispatchToProps = {
  onSetLogoutModalShown: authSetLogoutModalShown,
  onSetOnboardingUserTourStepAction: setOnboardingUserTourStepAction,
  onSetOnboardingUserTourShown: setOnboardingUserTourShownAction,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HeaderMenu);
