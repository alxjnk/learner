import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
  authLogout,
  authSetLogoutModalShown,
} from '../../modules/auth/actions';
import { getIsLogoutModalShownSelector } from '../../modules/auth/selectors';
import { ModalWindow } from '../../my-custom-semantic-theme/components/modal-window';

function LogoutModalComponent({ onSetModalShown, isModalShown, onLogout }) {
  const { t } = useTranslation();

  const handleCloseModal = () => {
    onSetModalShown(false);
  };

  const handleLogout = () => {
    onLogout();
  };
  return (
    <ModalWindow
      isModalShown={isModalShown}
      onClose={handleCloseModal}
      isNarrow
      actionButtons={[
        { color: 'secondary', text: t('Cancel'), onClick: handleCloseModal },
        { color: 'red', text: t('logOut'), onClick: handleLogout },
      ]}
      size="mini"
      headerText={t('Confirm log out')}
    />
  );
}

LogoutModalComponent.propTypes = {
  onSetModalShown: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
  isModalShown: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isModalShown: state => getIsLogoutModalShownSelector(state),
});

const mapDispatchToProps = {
  onSetModalShown: authSetLogoutModalShown,
  onLogout: authLogout,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const LogoutModal = compose(withConnect)(LogoutModalComponent);

export { LogoutModal };
