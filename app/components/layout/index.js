/*
 *
 * Header component
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import { Dropdown } from 'semantic-ui-react';
// import { ChevronLeft, Menu, X } from 'tabler-icons-react';
// import Logo from '../../assets/img/Logo-small.svg';
import './styles.less';

export function Layout({ header, footer, content, footerHeight }) {
  const contentStylesWhenFooterIsThrown = footer
    ? { paddingBottom: `calc(${footerHeight} + 5px)` }
    : {};
  return (
    <div className="layout">
      {header && <div className="layout__header">{header}</div>}
      <div className="layout__content" style={contentStylesWhenFooterIsThrown}>
        {content}
      </div>
      {footer && (
        <div
          className="layout__footer"
          style={{ top: `calc(100% - ${footerHeight})` }}
        >
          {footer}
        </div>
      )}
    </div>
  );
}

Layout.propTypes = {
  header: PropTypes.element.isRequired,
  footer: PropTypes.element,
  content: PropTypes.element.isRequired,
  footerHeight: PropTypes.string,
};
