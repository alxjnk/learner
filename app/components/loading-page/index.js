import React from 'react';
import { ToolsKitchen } from 'tabler-icons-react';
import './styles.less';

function LoadingPage() {
  return (
    <div className="loading-page">
      <div className="loading-page__logo-wrapper">
        <ToolsKitchen className="loading-page__logo" />
      </div>
      <div className="loading-page__app-name">Learner</div>
      <div className="loading-page__loading">Loading...</div>
    </div>
  );
}

export { LoadingPage };
