import React from "react";
import PropTypes from "prop-types";

import "./styles.less";

function MessageInsteadOfContent({ message }) {
  return <p className="content-message">{message}</p>;
}

MessageInsteadOfContent.propTypes = { message: PropTypes.string.isRequired };

export { MessageInsteadOfContent };
