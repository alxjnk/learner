import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { MobileListItem } from '../../my-custom-semantic-theme/components/mobile-list-item';
import './styles.less';
import { CustomLink } from '../../my-custom-semantic-theme/components/custom-link';

function MobileListWithDate({ data, icon }) {
  const { t } = useTranslation();
  return (
    <div className="mobile-list-with-date">
      {data.map(item => (
        <React.Fragment key={item.id}>
          {item.menuLink ? (
            <CustomLink
              text={item.shouldTranslate ? t(item.groupText) : item.groupText}
              to={item.menuLink}
            />
          ) : (
            <p className="mobile-list-with-date__item-time">
              {item.shouldTranslate ? t(item.groupText) : item.groupText}
            </p>
          )}
          <Link to={item.to}>
            <MobileListItem
              avatarText={item.avatarText}
              avatar={item.avatar}
              img={item.avatarImage}
              title={item.title}
              subText={item.subText}
              icon={icon}
            />
          </Link>
        </React.Fragment>
      ))}
    </div>
  );
}

MobileListWithDate.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      img: PropTypes.string,
      subText: PropTypes.string.isRequired,
      text: PropTypes.string,
      title: PropTypes.string.isRequired,
      to: PropTypes.string.isRequired,
      groupText: PropTypes.string,
    }),
  ),
  icon: PropTypes.element,
};

export { MobileListWithDate };
