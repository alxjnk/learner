/* eslint-disable no-useless-escape */
import { headers, KITCHEN_API_URL } from '../../utils/api/constants';

/* eslint-disable no-plusplus */
function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

export function subscribeUserToPush(userData, token) {
  return navigator.serviceWorker
    .register('/sw.js')
    .then(registration => {
      const subscribeOptions = {
        userVisibleOnly: true,
        applicationServerKey: urlBase64ToUint8Array(
          'BN456ScxRpQjGwQJMHlBG6mk1m2TJpeduwnMH7g-2C9PpAPZ4Qf1V1aU0Nx7M9j_KUFfvXt9X7JUrgjqzcUP9m4',
        ),
      };
      return registration.pushManager.subscribe(subscribeOptions);
    })
    .then(pushSubscription => {
      const pushSubscriptionData = JSON.parse(JSON.stringify(pushSubscription));
      fetch(`${KITCHEN_API_URL}/web-push/subscription`, {
        method: 'POST',
        body: JSON.stringify({
          role: userData.user.role,
          id: userData.user.id,
          ...pushSubscriptionData,
        }),
        headers: { ...headers, Authorization: `Bearer ${token.access.token}` },
      });
    });
}
