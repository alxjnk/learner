/* eslint-disable consistent-return */
import { askPermission } from './ask-permission';
import { checkIsSupported } from './check-support';
import { subscribeUserToPush } from './subscribe-user-to-push';

export function registerServiceWorker(userData, token, onSetReceiveNewSW) {
  const isSupported = checkIsSupported();
  if (!isSupported) return;
  askPermission();
  navigator.serviceWorker.addEventListener('controllerchange', () => {
    onSetReceiveNewSW(true);
  });
  return navigator.serviceWorker
    .register('/sw.js')
    .then(registration => registration)
    .then(subscribeUserToPush(userData, token))
    .catch(err => console.error('Unable to register service worker.', err));
}
