const NOTIFICATION = 'KITCHEN/NOTIFICATION/';
export const NOTIFICATION_INIT = `${NOTIFICATION}NOTIFICATION_INIT`;
export const NOTIFICATION_DISMISS = `${NOTIFICATION}NOTIFICATION_DISMISS`;
