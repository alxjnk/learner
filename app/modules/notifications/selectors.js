import { createSelector } from 'reselect';
import { defaultState } from './reducer';

// export const getIsLoading = state => state.auth.isLoading;
// export const getIsAuthorized = state => state.auth.isAuthorized;
// export const getToken = state => state.auth.token;

const selectReportsPageDomain = state => state.notifications || defaultState;

const getNotificationSelector = () =>
  createSelector(
    selectReportsPageDomain,
    ({ notifications }) => notifications,
  );

export { getNotificationSelector };
