import React from 'react';
import PropTypes from 'prop-types';
import { ErrorBoundaryView } from '../../my-custom-semantic-theme/components/error-boundary-view';

class ErrorBoundary extends React.Component {
  state = { hasError: false, errorMessage: '' };

  static getDerivedStateFromError(error) {
    return { hasError: true, errorMessage: error.message };
  }

  handleRefreshPage = () => {
    this.reset();
    this.props.onRefreshPage();
  };

  reset() {
    this.setState({ hasError: false, errorMessage: '' });
  }

  render() {
    if (this.state.hasError) {
      return (
        <ErrorBoundaryView
          onRefreshPage={this.handleRefreshPage}
          errorMessage={this.state.errorMessage}
        />
      );
    }

    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  onRefreshPage: PropTypes.func.isRequired,
};

export { ErrorBoundary };
