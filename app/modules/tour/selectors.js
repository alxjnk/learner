import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectTourDomain = state => state.tour || initialState;

const getIsOnboardingUserTourShownSelector = () =>
  createSelector(
    selectTourDomain,
    ({ isOnboardingUserTourShown }) => isOnboardingUserTourShown,
  );

const getOnboardingUserTourStepSelector = () =>
  createSelector(
    selectTourDomain,
    ({ onboardingUserTourStep }) => onboardingUserTourStep,
  );

const getIsOneOfTourShownSelector = () =>
  createSelector(
    selectTourDomain,
    ({ isOnboardingUserTourShown }) => isOnboardingUserTourShown,
  );

export default selectTourDomain;
export {
  getIsOnboardingUserTourShownSelector,
  getOnboardingUserTourStepSelector,
  getIsOneOfTourShownSelector,
};
