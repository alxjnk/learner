export const SET_ONBOARDING_USER_TOUR_SHOWN =
  'app/Tour/SET_ONBOARDING_USER_TOUR_SHOWN';
export function setOnboardingUserTourShownAction(payload) {
  return {
    type: SET_ONBOARDING_USER_TOUR_SHOWN,
    payload,
  };
}

export const SET_ONBOARDING_USER_TOUR_STEP =
  'app/Tour/SET_ONBOARDING_USER_TOUR_STEP';
export function setOnboardingUserTourStepAction(payload) {
  return {
    type: SET_ONBOARDING_USER_TOUR_STEP,
    payload,
  };
}

export const SET_ONBOARDING_CHEF_TOUR_SHOWN =
  'app/Tour/SET_ONBOARDING_CHEF_TOUR_SHOWN';
export function setOnboardingChefTourShownAction(payload) {
  return {
    type: SET_ONBOARDING_CHEF_TOUR_SHOWN,
    payload,
  };
}

export const SET_ONBOARDING_CHEF_TOUR_STEP =
  'app/Tour/SET_ONBOARDING_CHEF_TOUR_STEP';
export function setOnboardingChefTourStepAction(payload) {
  return {
    type: SET_ONBOARDING_CHEF_TOUR_STEP,
    payload,
  };
}
