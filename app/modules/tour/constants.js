const isTourWasShownBefore = () =>
  Boolean(localStorage.getItem('isTourWasShown'));

const setTourWasShownBefore = () =>
  localStorage.setItem('isTourWasShown', true);

export { isTourWasShownBefore, setTourWasShownBefore };
