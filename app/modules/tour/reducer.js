import {
  SET_ONBOARDING_CHEF_TOUR_SHOWN,
  SET_ONBOARDING_CHEF_TOUR_STEP,
  SET_ONBOARDING_USER_TOUR_SHOWN,
  SET_ONBOARDING_USER_TOUR_STEP,
} from './actions';

export const initialState = {
  isOnboardingUserTourShown: false,
  onboardingUserTourStep: 0,
  isOnboardingChefTourShown: false,
  onboardingChefTourStep: 0,
};

function tourReducer(state = initialState, action) {
  switch (action.type) {
    case SET_ONBOARDING_USER_TOUR_SHOWN:
      return {
        ...state,
        isOnboardingUserTourShown: action.payload,
        onboardingUserTourStep: 0,
      };
    case SET_ONBOARDING_USER_TOUR_STEP:
      return { ...state, onboardingUserTourStep: action.payload };
    case SET_ONBOARDING_CHEF_TOUR_SHOWN:
      return {
        ...state,
        isOnboardingChefTourShown: action.payload,
        onboardingChefTourStep: 0,
      };
    case SET_ONBOARDING_CHEF_TOUR_STEP:
      return { ...state, onboardingChefTourStep: action.payload };
    default:
      return state;
  }
}

export default tourReducer;
