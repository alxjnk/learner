import { call, put, select } from 'redux-saga/effects';
import { v4 as uuidv4 } from 'uuid';
import { push } from 'react-router-redux';
import moment from 'moment';
import {
  authSuccess,
  authFailure,
  authLogout,
  authRefreshToken,
  authLogoutComplete,
} from './actions';
import { getUsersAction } from '../../containers/BooksPage/actions';
import { TRANSPORT_ERROR_MSG } from '../../utils/api/constants';
import { authRefreshTokenRequest, authRequest } from '../../utils/api/requests';
import { notificationInit } from '../notifications/actions';
import { getTokens, getAuth } from './selectors';
import { setLocalStorageState } from '../../utils/localStorage';
import i18n from '../../containers/translations/i18n';

export default function* handleAuthorization({ payload }) {
  try {
    const { tokens, message, user } = yield call(authRequest, payload);
    if (message) {
      yield put(
        notificationInit({
          id: uuidv4(),
          message: i18n.t(message),
          type: 'warning',
          dismissAfter: 6000,
        }),
      );
      yield put(authFailure());
      return;
    }
    yield put(authSuccess({ tokens, user }));
    yield put(push('/'));
  } catch (error) {
    yield put(
      notificationInit({
        id: uuidv4(),
        message: i18n.t(error.message),
        type: 'warning',
        dismissAfter: 10000,
      }),
    );
    yield put(authFailure(TRANSPORT_ERROR_MSG + error.message));
  }
}

export function* handleRefreshToken({ payload }) {
  try {
    const { refresh } = yield select(getTokens);
    if (!moment().isBefore(refresh.expires)) {
      yield put(authLogout());
      return;
    }

    const tokens = yield call(authRefreshTokenRequest, {
      refreshToken: refresh.token,
    });
    if (tokens.message) {
      yield put(
        notificationInit({
          id: uuidv4(),
          message: tokens.message,
          type: 'warning',
          dismissAfter: 3000,
        }),
      );
      yield put(authLogout());
      return;
    }

    yield put(authRefreshToken(tokens));
    const auth = yield select(getAuth);
    setLocalStorageState({
      auth: {
        tokens: auth.tokens,
        isAuthorized: auth.isAuthorized,
        user: auth.user,
      },
    });
  } catch (error) {
    yield put(
      notificationInit({
        id: uuidv4(),
        message: TRANSPORT_ERROR_MSG + error.message,
        type: 'warning',
        dismissAfter: 3000,
      }),
    );
  } finally {
    if (payload?.data === 'userRequest') {
      yield put(getUsersAction());
    }
  }
}

export function* handleLogOutSaga() {
  try {
    yield put(authLogoutComplete());
  } catch (error) {
    yield put(
      notificationInit({
        id: uuidv4(),
        message: error.message,
        type: 'warning',
        dismissAfter: 6000,
      }),
    );
  }
}
