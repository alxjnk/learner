import * as actions from './actionTypes';

export const authRequest = data => ({
  type: actions.AUTH_REQUEST,
  payload: data,
});

export const authSocketConnectionComplete = () => ({
  type: actions.AUTH_SOCKET_CONNECTION_COMPLETE,
});

export const authSuccess = authData => ({
  type: actions.AUTH_SUCCESS,
  payload: authData,
});

export const authFailure = errorMessage => ({
  type: actions.AUTH_FAILURE,
  payload: errorMessage,
});

export const authLogout = () => ({
  type: actions.AUTH_LOGOUT,
});

export const authLogoutComplete = () => ({
  type: actions.AUTH_LOGOUT_COMPLETE,
});

export const authRefreshToken = payload => ({
  type: actions.AUTH_REFRESH_TOKEN,
  payload,
});

export const authRefreshTokenRequest = payload => ({
  type: actions.AUTH_REFRESH_TOKEN_REQUEST,
  payload,
});

export const authSetLogoutModalShown = payload => ({
  type: actions.AUTH_LOGOUT_MODAL_SHOWN,
  payload,
});
