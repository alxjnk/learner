export const getIsLoading = state => state.auth.isLoading;
export const getIsAuthorized = state => state.auth.isAuthorized;
export const getTokens = state => state.auth.tokens;
export const getUser = state => state.auth.user;
export const getErrors = state => state.auth.errors;
export const getUserName = state => state.auth.user.name;
export const getIsUserAdmin = state =>
  Boolean(state.auth.user && state.auth.user.role === 'admin');
export const getIsUserStore = state =>
  Boolean(state.auth.user && state.auth.user.role === 'store');
export const getIsUser = state =>
  Boolean(state.auth.user && state.auth.user.role === 'user');
export const getAuth = state => state.auth;
export const getIsLogoutModalShownSelector = state =>
  Boolean(state.auth.isLogoutModalShown);
