import * as actions from './actionTypes';

const defaultState = {
  isLoading: false,
  isAuthorized: false,
  errors: null,
  tokens: null,
  user: null,
  isLogoutModalShown: false,
};

const authReducer = (state = defaultState, action) => {
  const { type, payload } = action;
  switch (type) {
    case actions.AUTH_REQUEST:
      return {
        ...state,
        isLoading: true,
        errors: null,
        tokens: null,
        user: null,
      };
    case actions.AUTH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuthorized: true,
        errors: null,
        tokens: payload.tokens,
        user: payload.user,
      };
    case actions.AUTH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isAuthorized: false,
        tokens: null,
        users: null,
      };
    case actions.AUTH_LOGOUT_COMPLETE:
      return defaultState;
    case actions.AUTH_REFRESH_TOKEN:
      return {
        ...state,
        tokens: payload,
      };
    case actions.AUTH_LOGOUT_MODAL_SHOWN:
      return {
        ...state,
        isLogoutModalShown: payload,
      };
    default:
      return state;
  }
};

export default authReducer;
