import img from './images/icon-512x512.png';

self.addEventListener('push', event => {
  const data = event.data.json();

  event.waitUntil(
    self.registration.showNotification('Kitchen app', {
      icon: img,
      tag: data.body,
      body: data.body,
    }),
  );
});

self.addEventListener('install', event => {
  self.skipWaiting();
});
