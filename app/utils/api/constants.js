// let subdomain;
// switch (window.location.host.split('.')[0]) {
//   case 'chefapp':
//     subdomain = 'chefapp';
//     break;
//   case 'kitchenapp':
//     subdomain = 'chefapp';
//     break;
//   default:
//     subdomain = 'v1';
//     break;
// }

export const KITCHEN_API_URL =
  process.env.NODE_ENV === 'production'
    ? `${window.location.origin}/chefapp`
    : 'http://77.232.42.37:3000/v1';
// : 'http://localhost:3002/v1';

export const KITCHEN_SOCKET_API_URL =
  process.env.NODE_ENV === 'production'
    ? `${window.location.origin}`
    : 'http://77.232.42.37:3000';
// :  'http://localhost:3002';

export const KITCHEN_IMAGE_API_URL =
  process.env.NODE_ENV === 'production'
    ? `${window.location.origin}/uploads`
    : 'https://test-kitchenapp.jobsdone.pro:4430/uploads';

export const TRANSPORT_ERROR_MSG = 'Не удалось получить данные от сервера: ';
export const GET = 'GET';
export const POST = 'POST';
export const DELETE = 'DELETE';
export const PUT = 'PUT';
export const PATCH = 'PATCH';
export const headers = { 'Content-Type': 'application/json' };
