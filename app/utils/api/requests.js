import { KITCHEN_API_URL, POST, headers, GET, PATCH } from './constants';
import { responseErrorHandler } from '../responseErrorHandler';

const authRequest = request =>
  fetch(`${KITCHEN_API_URL}/auth/login`, {
    method: POST,
    headers,
    body: JSON.stringify(request),
  }).then(responseErrorHandler);

const authRefreshTokenRequest = token =>
  fetch(`${KITCHEN_API_URL}/auth/refresh-tokens`, {
    method: POST,
    headers,
    body: JSON.stringify(token),
  }).then(responseErrorHandler);

const postStocksRequest = (body, token) =>
  fetch(`${KITCHEN_API_URL}/stocks`, {
    method: POST,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const getIngredientsRequest = token =>
  fetch(`${KITCHEN_API_URL}/ingredients?limit=10000`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const getUserRequest = (id, token) =>
  fetch(`${KITCHEN_API_URL}/users/${id}`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const getUsersListRequest = token =>
  fetch(`${KITCHEN_API_URL}/users`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const getReceiptsListRequest = token =>
  fetch(`${KITCHEN_API_URL}/receipts?limit=10000`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const getReceiptRequest = (id, token) =>
  fetch(`${KITCHEN_API_URL}/receipts/${id}`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const getSectionsRequest = token =>
  fetch(`${KITCHEN_API_URL}/sections?limit10000`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const postTaskRequest = (body, token) =>
  fetch(`${KITCHEN_API_URL}/tasks`, {
    method: POST,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const getSectionRequest = (id, token) =>
  fetch(`${KITCHEN_API_URL}/sections/${id}`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const patchSectionWorkersRequest = (body, id, token) =>
  fetch(`${KITCHEN_API_URL}/sections/${id}`, {
    method: PATCH,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const patchUserSectionIdRequest = (body, id, token) =>
  fetch(`${KITCHEN_API_URL}/users/${id}`, {
    method: PATCH,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const patchTaskRequest = (body, id, token) =>
  fetch(`${KITCHEN_API_URL}/tasks/${id}`, {
    method: PATCH,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const postDailyPlanRequest = (body, token) =>
  fetch(`${KITCHEN_API_URL}/daily-plan`, {
    method: POST,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const getDailyPlanRequest = (date, token) =>
  fetch(`${KITCHEN_API_URL}/daily-plan${date}`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const postProductOrdersRequest = (body, token) =>
  fetch(`${KITCHEN_API_URL}/product-orders`, {
    method: POST,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const getProductsOrdersRequest = (orderDay, sectionId, token) =>
  fetch(
    `${KITCHEN_API_URL}/product-orders?orderDay=${orderDay}&sectionId=${sectionId}`,
    {
      method: GET,
      headers: { ...headers, Authorization: `Bearer ${token}` },
    },
  ).then(responseErrorHandler);

const getStocksBySectionAndDateRequest = (token, from, to, sectionId) =>
  fetch(
    `${KITCHEN_API_URL}/stocks?from=${from}&to=${to}&sectionId=${sectionId}`,
    {
      method: GET,
      headers: { ...headers, Authorization: `Bearer ${token}` },
    },
  ).then(responseErrorHandler);

const getUsersShiftsByDateRequest = (token, from, to, userId) =>
  fetch(`${KITCHEN_API_URL}/shifts?from=${from}&to=${to}&userId=${userId}`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const deleteShiftsRequest = (body, token) =>
  fetch(`${KITCHEN_API_URL}/shifts`, {
    method: PATCH,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const postShiftsRequest = (body, token) =>
  fetch(`${KITCHEN_API_URL}/shifts`, {
    method: POST,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const getScheduleChangeRequest = (params, token) =>
  fetch(`${KITCHEN_API_URL}/schedule-change?${params}`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const postScheduleChangeRequest = (body, token) =>
  fetch(`${KITCHEN_API_URL}/schedule-change`, {
    method: POST,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const patchScheduleChangeRequest = (body, token) =>
  fetch(`${KITCHEN_API_URL}/schedule-change`, {
    method: PATCH,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const postPushRequest = (body, token, query) =>
  fetch(`${KITCHEN_API_URL}/web-push${query || ''}`, {
    method: POST,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  });

const getTasksHistoryRequest = (params, token) =>
  fetch(`${KITCHEN_API_URL}/tasks/history${params}`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const countStocksRequest = (body, token) =>
  fetch(`${KITCHEN_API_URL}/stocks/count`, {
    method: POST,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

const getCheckListsRequest = token =>
  fetch(`${KITCHEN_API_URL}/check-lists`, {
    method: GET,
    headers: { ...headers, Authorization: `Bearer ${token}` },
  }).then(responseErrorHandler);

const patchCheckListRequest = (body, id, token) =>
  fetch(`${KITCHEN_API_URL}/check-lists/${id}`, {
    method: PATCH,
    headers: { ...headers, Authorization: `Bearer ${token}` },
    body: JSON.stringify(body),
  }).then(responseErrorHandler);

export {
  authRequest,
  getSectionsRequest,
  postStocksRequest,
  getIngredientsRequest,
  getUserRequest,
  getUsersListRequest,
  getReceiptsListRequest,
  getReceiptRequest,
  postTaskRequest,
  patchSectionWorkersRequest,
  getSectionRequest,
  patchUserSectionIdRequest,
  patchTaskRequest,
  postDailyPlanRequest,
  getDailyPlanRequest,
  postProductOrdersRequest,
  getProductsOrdersRequest,
  getStocksBySectionAndDateRequest,
  getUsersShiftsByDateRequest,
  deleteShiftsRequest,
  authRefreshTokenRequest,
  getScheduleChangeRequest,
  postScheduleChangeRequest,
  patchScheduleChangeRequest,
  postShiftsRequest,
  postPushRequest,
  getTasksHistoryRequest,
  countStocksRequest,
  getCheckListsRequest,
  patchCheckListRequest,
};
