import {
  ChartBar,
  List,
  ShoppingCart,
  ToolsKitchen2,
  Users,
  Bell,
} from 'tabler-icons-react';

export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';
export const tasksAndStocksTabs = [
  { name: 'tasks', icon: List },
  { name: 'stocks', icon: ShoppingCart },
  { name: 'notifications', icon: Bell },
];
export const sectionAndStatsTabs = [
  { name: 'section', icon: Users },
  { name: 'stats', icon: ChartBar },
  { name: 'sections-tasks', icon: ToolsKitchen2 },
  { name: 'notifications', icon: Bell },
];

export const MobileTabsColors = { active: '#3184FF', disabled: '#B5B5B7' };
