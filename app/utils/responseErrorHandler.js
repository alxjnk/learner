const responseErrorHandler = async response => {
  const responseJson = await response.json();
  if (responseJson.message) {
    throw new Error(responseJson.message);
  }
  return responseJson;
};

export { responseErrorHandler };
