/**
 * Combine all reducers in this file and export the combined reducers.
 */
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from 'utils/history';
import { reducer as formReducer } from 'redux-form';
import notificationsReducer from './modules/notifications/reducer';
import authReducer from './modules/auth/reducer';
import tourReducer from './modules/tour/reducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  return combineReducers({
    auth: authReducer,
    router: connectRouter(history),
    notifications: notificationsReducer,
    form: formReducer,
    tour: tourReducer,
    ...injectedReducers,
  });
}
